<?php

namespace App\Tests\Controller\Servants;

use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServantGlobalStatistics extends WebTestCase
{
    /**
     * Cette fonction permet de tester la récupération de la liste des servants si l'utilisateur est connecté
     *
     * Doit retourner un code 200
     * @return void
     * @throws Exception
     */
    public function testGetGlobalServantsStatisticsApiIfUserLogged() : void
    {

        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $response = $client->request("GET", "/servants/global-statistics", []);

        $this->assertResponseIsSuccessful();
    }

    /**
     * Cette fonction permet de tester la récupération de la liste des servants si l'utilisateur n'est pas connecté
     *
     * Doit retourner un code 401
     * @return void
     * @throws Exception
     */
    public function testGetGlobalrServantsStatisticsIfUserNotLogged() : void
    {
        $client = static::createClient();

        $response = $client->request("GET", "/servants/global-statistics");

        $this->assertResponseStatusCodeSame(401);
    }
}
