<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class User2FaRequest extends AbstractRequest
{
    #[SerializedName('multiFa')]
    #[Assert\Type(
        type: 'bool',
        message: 'The value {{ value }} is not a valid {{ type }}.',
    )]
    private bool $multiFa;

    public function isMultiFa(): bool
    {
        return $this->multiFa;
    }

    public function setMultiFa(bool $multiFa): void
    {
        $this->multiFa = $multiFa;
    }


}
