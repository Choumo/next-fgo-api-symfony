<?php

namespace App\Entity;

use App\Repository\ServantRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServantRepository::class)]
class Servant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    private ?int $servant_id = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 120)]
    private ?int $servant_level = 1;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private ?int $servant_level_skill_1 = 1;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private ?int $servant_level_skill_2 = 1;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private ?int $servant_level_skill_3 = 1;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private ?int $servant_level_append_1 = 1;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private ?int $servant_level_append_2 = 1;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private ?int $servant_level_append_3 = 1;

    #[ORM\ManyToOne(inversedBy: 'servants')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    // TODO : Modifier nullable
    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 15)]
    private ?int $servant_bond_level = 1;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 5)]
    private ?int $servant_np_level = 1;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServantId(): ?int
    {
        return $this->servant_id;
    }

    public function setServantId(int $servant_id): static
    {
        $this->servant_id = htmlspecialchars($servant_id);

        return $this;
    }

    public function getServantLevel(): ?int
    {
        return $this->servant_level;
    }

    public function setServantLevel(int $servant_level): static
    {
        $this->servant_level = htmlspecialchars($servant_level);

        return $this;
    }

    public function getServantLevelSkill1(): ?int
    {
        return $this->servant_level_skill_1;
    }

    public function setServantLevelSkill1(int $servant_level_skill_1): static
    {
        $this->servant_level_skill_1 = htmlspecialchars($servant_level_skill_1);

        return $this;
    }

    public function getServantLevelSkill2(): ?int
    {
        return $this->servant_level_skill_2;
    }

    public function setServantLevelSkill2(int $servant_level_skill_2): static
    {
        $this->servant_level_skill_2 = htmlspecialchars($servant_level_skill_2);

        return $this;
    }

    public function getServantLevelSkill3(): ?int
    {
        return $this->servant_level_skill_3;
    }

    public function setServantLevelSkill3(int $servant_level_skill_3): static
    {
        $this->servant_level_skill_3 = htmlspecialchars($servant_level_skill_3);

        return $this;
    }

    public function getServantLevelAppend1(): ?int
    {
        return $this->servant_level_append_1;
    }

    public function setServantLevelAppend1(int $servant_level_append_1): static
    {
        $this->servant_level_append_1 = htmlspecialchars($servant_level_append_1);

        return $this;
    }

    public function getServantLevelAppend2(): ?int
    {
        return $this->servant_level_append_2;
    }

    public function setServantLevelAppend2(int $servant_level_append_2): static
    {
        $this->servant_level_append_2 = htmlspecialchars($servant_level_append_2);

        return $this;
    }

    public function getServantLevelAppend3(): ?int
    {
        return $this->servant_level_append_3;
    }

    public function setServantLevelAppend3(int $servant_level_append_3): static
    {
        $this->servant_level_append_3 = htmlspecialchars($servant_level_append_3);

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getServantBondLevel(): ?int
    {
        return $this->servant_bond_level;
    }

    public function setServantBondLevel(int $servant_bond_level): static
    {
        $this->servant_bond_level = htmlspecialchars($servant_bond_level);

        return $this;
    }

    public function getServantNpLevel(): ?int
    {
        return $this->servant_np_level;
    }

    public function setServantNpLevel(int $servant_np_level): static
    {
        $this->servant_np_level = htmlspecialchars($servant_np_level);

        return $this;
    }
}
