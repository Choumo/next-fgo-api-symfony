<?php

namespace App\Tests\Controller\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SignUpTest extends WebTestCase
{
    public function testResgisterSuccess(): void
    {

        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode([
                'email' => "email3@provider.fr",
                'password' => "NqR1frt98M8Z88PH9SpZ0GbHj9ZQh",
                'passwordCheck' => "NqR1frt98M8Z88PH9SpZ0GbHj9ZQh",
            ])
        );

        $this->assertResponseIsSuccessful();
    }

    public function testResgisterEmailAlreadyExists(): void
    {
        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode([
                'email' => "email2@provider.fr",
                'password' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh',
                'passwordCheck' => "NqR1frt98M8Z88PH9SpZ0GbHj9ZQh",
            ])
        );

        $this->assertResponseStatusCodeSame(422);
    }

    public function testRegisterFailureWithNoParams(): void
    {

        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register'
        );

        $this->assertResponseStatusCodeSame(422);
    }


    public function testRegisterFailureWithMissingEmail(): void
    {

        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode(
                [
                    'password' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh',
                    'passwordCheck' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh'
                ]
            )
        );

        $this->assertResponseStatusCodeSame(422);
    }


    public function testRegisterFailureWithMissingPassword(): void
    {

        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode(
                [
                    'email' => 'email@provider.fr',
                    'passwordCheck' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh'
                ])
        );

        $this->assertResponseStatusCodeSame(422);
    }


    public function testRegisterFailureWithMissingPasswordCheck(): void
    {
        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode(
                [
                    'password' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh',
                    'email' => 'email@provider.fr'
                ]
            )
        );

        $this->assertResponseStatusCodeSame(422);
    }


    public function testRegisterFailureWithEmailNotValid(): void
    {
        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode(
                [
                    'email' => 'email@providerr',
                    'password' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh',
                    'passwordCheck' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh'
                ]
            )
        );

        $this->assertResponseStatusCodeSame(422);
    }


    public function testRegisterFailureWithPasswordNotValid(): void
    {
        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode(
                [
                    'email' => 'email@providerr',
                    'password' => 'somepassword',
                    'passwordCheck' => 'somepassword'
                ]
            )
        );

        $this->assertResponseStatusCodeSame(422);
    }


    public function testRegisterFailureWithPasswordsNotTheSame(): void
    {
        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/register',
            content: json_encode(
                [
                    'email' => 'email@providerr',
                    'password' => 'NqR1frt98M8Z88PH9SpZ0GbHj9ZQh',
                    'passwordCheck' => 'SomeP4ssW0rd!'
                ]
            )
        );

        $this->assertResponseStatusCodeSame(422);
    }
}
