<?php

namespace App\Factory\Request\ManagerRequest;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
class TargetsObject
{
    #[SerializedName('skill1')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $skill1;

    #[SerializedName('skill2')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $skill2;

    #[SerializedName('skill3')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $skill3;

    #[SerializedName('append1')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $append1;

    #[SerializedName('append2')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $append2;

    #[SerializedName('append3')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $append3;

    /**
     * @return int
     */
    public function getSkill1(): int
    {
        return $this->skill1;
    }

    /**
     * @param int $skill1
     */
    public function setSkill1(int $skill1): void
    {
        $this->skill1 = $skill1;
    }

    /**
     * @return int
     */
    public function getSkill2(): int
    {
        return $this->skill2;
    }

    /**
     * @param int $skill2
     */
    public function setSkill2(int $skill2): void
    {
        $this->skill2 = $skill2;
    }

    /**
     * @return int
     */
    public function getSkill3(): int
    {
        return $this->skill3;
    }

    /**
     * @param int $skill3
     */
    public function setSkill3(int $skill3): void
    {
        $this->skill3 = $skill3;
    }

    /**
     * @return int
     */
    public function getAppend1(): int
    {
        return $this->append1;
    }

    /**
     * @param int $append1
     */
    public function setAppend1(int $append1): void
    {
        $this->append1 = $append1;
    }

    /**
     * @return int
     */
    public function getAppend2(): int
    {
        return $this->append2;
    }

    /**
     * @param int $append2
     */
    public function setAppend2(int $append2): void
    {
        $this->append2 = $append2;
    }

    /**
     * @return int
     */
    public function getAppend3(): int
    {
        return $this->append3;
    }

    /**
     * @param int $append3
     */
    public function setAppend3(int $append3): void
    {
        $this->append3 = $append3;
    }

    /**
     * @param string $key
     * @return int|void
     */
    public function get(string $key) {
        switch ($key) {
            case 'skill1':
                return $this->skill1;
            case 'skill2':
                return $this->skill2;
            case 'skill3':
                return $this->skill3;
            case 'append1':
                return $this->append1;
            case 'append2':
                return $this->append2;
            case 'append3':
                return $this->append3;
        }
    }

}
