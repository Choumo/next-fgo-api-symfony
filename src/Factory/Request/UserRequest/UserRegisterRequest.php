<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
class UserRegisterRequest extends AbstractRequest
{
    #[SerializedName('email')]
    #[Assert\Email]
    #[Assert\NotBlank]
    private string $email;

    #[SerializedName('password')]
    #[Assert\PasswordStrength(['minScore' => Assert\PasswordStrength::STRENGTH_MEDIUM])]
    #[Assert\NotBlank]
    private string $password;

    #[SerializedName('passwordCheck')]
    #[Assert\EqualTo(propertyPath: 'password')]
    #[Assert\NotBlank]
    private string $passwordCheck;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPasswordCheck(): string
    {
        return $this->passwordCheck;
    }

    /**
     * @param string $passwordCheck
     */
    public function setPasswordCheck(string $passwordCheck): void
    {
        $this->passwordCheck = $passwordCheck;
    }


}
