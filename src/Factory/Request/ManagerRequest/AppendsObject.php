<?php

namespace App\Factory\Request\ManagerRequest;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class AppendsObject
{
    #[SerializedName('append1')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $append1;

    #[SerializedName('append2')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $append2;

    #[SerializedName('append3')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    public int $append3;

    /**
     * @return int
     */
    public function getAppend1(): int
    {
        return $this->append1;
    }

    /**
     * @param int $append1
     */
    public function setAppend1(int $append1): void
    {
        $this->append1 = $append1;
    }

    /**
     * @return int
     */
    public function getAppend2(): int
    {
        return $this->append2;
    }

    /**
     * @param int $append2
     */
    public function setAppend2(int $append2): void
    {
        $this->append2 = $append2;
    }

    /**
     * @return int
     */
    public function getAppend3(): int
    {
        return $this->append3;
    }

    /**
     * @param int $append3
     */
    public function setAppend3(int $append3): void
    {
        $this->append3 = $append3;
    }

    /**
     * @param string $key
     * @return int|void
     */
    public function get(string $key) {
        switch ($key) {
            case 'append1':
                return $this->append1;
            case 'append2':
                return $this->append2;
            case 'append3':
                return $this->append3;
        }
    }

}
