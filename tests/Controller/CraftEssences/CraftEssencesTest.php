<?php

namespace App\Tests\Controller\CraftEssences;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class CraftEssencesTest extends WebTestCase
{
    public function testGetCraftEssencesFromApiIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $client->loginUser($user);
        $response = $client->request('GET', '/craft-essence/list');

        $this->assertResponseIsSuccessful();
    }


    public function testGetCraftEssencesFromApiIfUserNotLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $response = $client->request('GET', '/craft-essence/list');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testAddCraftEssenceToDatabaseIfUserNotLogged() : void 
    { 
        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $response = $client->request('POST', '/craft-essence/add/191');

        $this->assertResponseStatusCodeSame(401);
    }

    
    public function testAddCraftEssenceToDatabaseIfUserLogged() : void 
    { 
        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response = $client->request('POST', '/craft-essence/add/191');

        $this->assertResponseIsSuccessful();
    }

    public function testAddCraftEssenceToDatabaseIfCraftEssenceDoesNotExistsInApi(): void 
    {
        $client = static::createClient();    
        $container = static::getContainer();
        
        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $response = $client->request("POST", "/craft-essence/add/1006446874877", []);
        
        $this->assertResponseStatusCodeSame(401);
    } 
    //TODO : Test if craft essence id exists or not in the api
}
