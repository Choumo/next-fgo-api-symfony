<?php

namespace App\Factory\Request\ChapterRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class ChapterPostRequest extends AbstractRequest
{
    #[SerializedName('state')]
    #[Assert\Choice(
        choices: ["Not Done", "In Progress", "Cleared"]
    )]
    private string $state;

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): void
    {
        $this->state = $state;
    }


}
