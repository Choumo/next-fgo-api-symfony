<?php

namespace App\Service;

use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class JsonTransformer
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        //$extractor = new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]);
        //$this->serializer = new Serializer([new ObjectNormalizer(null, null, null, $extractor), new ArrayDenormalizer()], [new JsonEncoder()]);
         $this->serializer = $serializer;
    }

    /**
     * Fonction qui permet de deserialiser un objet JSON en objet PHP
     *
     * @param string $json
     * @param string $class
     * @return mixed|object|string|null
     */
    public function deserializeJson(string $json, string $class)
    {
        try {
            return $this->serializer->deserialize(
                data: $json,
                type: $class,
                format: 'json'
            );
        } catch (NotNormalizableValueException|NotEncodableValueException $exception) {
            //dd($exception->getMessage());
            return null;
        }
    }
}
