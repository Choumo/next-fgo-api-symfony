<?php

namespace App\Factory\Request\ManagerRequest;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class SkillsObject
{
    #[SerializedName('skill1')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    public int $skill1;

    #[SerializedName('skill2')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    public int $skill2;

    #[SerializedName('skill3')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    public int $skill3;

    /**
     * @return int
     */
    public function getSkill1(): int
    {
        return $this->skill1;
    }

    /**
     * @param int $skill1
     */
    public function setSkill1(int $skill1): void
    {
        $this->skill1 = $skill1;
    }

    /**
     * @return int
     */
    public function getSkill2(): int
    {
        return $this->skill2;
    }

    /**
     * @param int $skill2
     */
    public function setSkill2(int $skill2): void
    {
        $this->skill2 = $skill2;
    }

    /**
     * @return int
     */
    public function getSkill3(): int
    {
        return $this->skill3;
    }

    /**
     * @param int $skill3
     */
    public function setSkill3(int $skill3): void
    {
        $this->skill3 = $skill3;
    }

    public function get(string $key) {
        switch ($key) {
            case 'skill1':
                return $this->skill1;
            case 'skill2':
                return $this->skill2;
            case 'skill3':
                return $this->skill3;
        }
    }
}
