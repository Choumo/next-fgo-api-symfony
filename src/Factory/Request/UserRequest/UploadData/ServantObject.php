<?php

namespace App\Factory\Request\UserRequest\UploadData;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class ServantObject
{
    #[SerializedName('ServantId')]
    private ?int $servant_id = null;

    #[SerializedName('ServantLevel')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(120)]
    private ?int $servant_level = 1;

    #[SerializedName('ServantSkill1')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    private ?int $servant_level_skill_1 = 1;

    #[SerializedName('ServantSkill2')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    private ?int $servant_level_skill_2 = 1;

    #[SerializedName('ServantSkill3')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    private ?int $servant_level_skill_3 = 1;

    #[SerializedName('ServantAppend1')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    private ?int $servant_level_append_1 = 1;

    #[SerializedName('ServantAppend2')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    private ?int $servant_level_append_2 = 1;

    #[SerializedName('ServantAppend3')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(10)]
    private ?int $servant_level_append_3 = 1;


    public function getServantId(): ?int
    {
        return $this->servant_id;
    }

    public function setServantId(int $servant_id): static
    {
        $this->servant_id = htmlspecialchars($servant_id);

        return $this;
    }

    public function getServantLevel(): ?int
    {
        return $this->servant_level;
    }

    public function setServantLevel(int $servant_level): static
    {
        $this->servant_level = htmlspecialchars($servant_level);

        return $this;
    }

    public function getServantLevelSkill1(): ?int
    {
        return $this->servant_level_skill_1;
    }

    public function setServantLevelSkill1(int $servant_level_skill_1): static
    {
        $this->servant_level_skill_1 = htmlspecialchars($servant_level_skill_1);

        return $this;
    }

    public function getServantLevelSkill2(): ?int
    {
        return $this->servant_level_skill_2;
    }

    public function setServantLevelSkill2(int $servant_level_skill_2): static
    {
        $this->servant_level_skill_2 = htmlspecialchars($servant_level_skill_2);

        return $this;
    }

    public function getServantLevelSkill3(): ?int
    {
        return $this->servant_level_skill_3;
    }

    public function setServantLevelSkill3(int $servant_level_skill_3): static
    {
        $this->servant_level_skill_3 = htmlspecialchars($servant_level_skill_3);

        return $this;
    }

    public function getServantLevelAppend1(): ?int
    {
        return $this->servant_level_append_1;
    }

    public function setServantLevelAppend1(int $servant_level_append_1): static
    {
        $this->servant_level_append_1 = htmlspecialchars($servant_level_append_1);

        return $this;
    }

    public function getServantLevelAppend2(): ?int
    {
        return $this->servant_level_append_2;
    }

    public function setServantLevelAppend2(int $servant_level_append_2): static
    {
        $this->servant_level_append_2 = htmlspecialchars($servant_level_append_2);

        return $this;
    }

    public function getServantLevelAppend3(): ?int
    {
        return $this->servant_level_append_3;
    }

    public function setServantLevelAppend3(int $servant_level_append_3): static
    {
        $this->servant_level_append_3 = htmlspecialchars($servant_level_append_3);

        return $this;
    }

}
