<?php

namespace App\Security;

use App\Entity\User;
use App\Service\Token;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class AppAuthenticator extends AbstractAuthenticator
{
    private string $jwtSecretKey;
    private Token $tokenService;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, Token $tokenService, private LoggerInterface $logger){
        $this->jwtSecretKey = getenv('JWT_SECRET_KEY');
        $this->tokenService = $tokenService;
        $this->em = $em;
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('X-AUTH-TOKEN');
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function authenticate(Request $request): Passport
    {
        $api_token = $request->headers->get('X-AUTH-TOKEN');

        // Check if X-AUTH-TOKEN has been sent in the headers of the request
        if(null === $api_token){
            throw new CustomUserMessageAuthenticationException('No API token provided');
        }

        [$headers, $payload, $signature] = $this->tokenService->decodeJWT($api_token);

        // Check the validity of the token signature
        if(!$this->tokenService->verifySignature($headers, $payload, $signature) || time() >= $payload['exp']){
            throw new CustomUserMessageAuthenticationException('Something went wrong during the check of your API token');
        }

        $user_repository = $this->em->getRepository(User::class);
        $user = $user_repository->find($payload['sub']);

        if(!$user){
            throw new CustomUserMessageAuthenticationException('Invalid API Token');
        }

        return new SelfValidatingPassport(new UserBadge($user->getEmail()));

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, 401);
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
        {
            /*
             * If you would like this class to control what happens when an anonymous user accesses a
             * protected page (e.g. redirect to /login), uncomment this method and make this class
             * implement Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface.
             *
             * For more details, see https://symfony.com/doc/current/security/experimental_authenticators.html#configuring-the-authentication-entry-point
             */
            $data = ['message' => 'Authentication Required'];
            return new JsonResponse($data, 401);
        }
    }
