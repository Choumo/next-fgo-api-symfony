<?php

namespace App\Entity;

use App\Repository\MaterialRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MaterialRepository::class)]
class Material
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $material_id = null;

    #[ORM\Column]
    private ?int $material_quantity = null;

    #[ORM\ManyToOne(inversedBy: 'materials')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $material_user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaterialId(): ?int
    {
        return $this->material_id;
    }

    public function setMaterialId(int $material_id): static
    {
        $this->material_id = htmlspecialchars($material_id);

        return $this;
    }

    public function getMaterialQuantity(): ?int
    {
        return $this->material_quantity;
    }

    public function setMaterialQuantity(int $material_quantity): static
    {
        $this->material_quantity = htmlspecialchars($material_quantity);

        return $this;
    }

    public function getMaterialUser(): ?User
    {
        return $this->material_user;
    }

    public function setMaterialUser(?User $material_user): static
    {
        $this->material_user = $material_user;

        return $this;
    }
}
