<?php

namespace App\Entity;

use App\Repository\QuestRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuestRepository::class)]
class Quest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $chapter_id = null;

    #[ORM\Column]
    private ?string $chapter_state = null;

    #[ORM\ManyToOne(inversedBy: 'quests')]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChapterId(): ?int
    {
        return $this->chapter_id;
    }

    public function setChapterId(int $chapter_id): static
    {
        $this->chapter_id = htmlspecialchars($chapter_id);

        return $this;
    }

    public function getChapterState(): ?string
    {
        return $this->chapter_state;
    }

    public function setChapterState(?string $chapter_state): void
    {
        $this->chapter_state = htmlspecialchars($chapter_state);
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
