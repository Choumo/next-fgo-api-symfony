<?php

namespace App\Factory\Request\UserRequest\UploadData;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class UserObject
{
    #[SerializedName('email')]
    #[Assert\NotBlank()]
    private string $email;

    #[SerializedName('shareData')]
    #[Assert\NotBlank()]
    private bool $shareData;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isShareData(): bool
    {
        return $this->shareData;
    }

    /**
     * @param bool $shareData
     */
    public function setShareData(bool $shareData): void
    {
        $this->shareData = $shareData;
    }


}
