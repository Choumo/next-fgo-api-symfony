<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\ApiException;
use App\Exception\ApiValidationException;
use App\Factory\Request\Planner\PlannerPostRequest;
use App\Service\Api;
use App\Service\JsonTransformer;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/manager')]
class ManagerController extends AbstractController
{
    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}

    /**
     * Fonction qui permet de récupérer la liste des matériaux que l'utilisateur vas récupérer entre 2 dates sélectionnés
     *
     * @param Api $rayshiftApi
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/', name: 'app_manager_index', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function index(Api $rayshiftApi, Request $request): JsonResponse
    {
        try {
            /** @var PlannerPostRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: PlannerPostRequest::class);
            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $allEventsAndStory = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_war_lang_en.json', cacheKey: 'war-list');

            $startDateToTimeStamp = strtotime($requestPayload->getStartDate() . " -2 years");
            $endDateToTimeStamp = strtotime($requestPayload->getEndDate() . " -2 years");


            $daysBetweenStartAndEnd = ($endDateToTimeStamp - $startDateToTimeStamp) / 86400;

            $rewards = [];

            if($daysBetweenStartAndEnd < 0) {
                throw new Exception();
            }

            $loginStreakSaintQuartzBonus = floor($daysBetweenStartAndEnd / 50) * 30;
            $totalSqFragments = floor($daysBetweenStartAndEnd / 7) * 3;
            $totalSummonTicket = floor($daysBetweenStartAndEnd / 7);

            $sqFragmentInformation = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . 11 . '?lang=en' ,cacheKey: 'material-' . 11);

            $rewards[$sqFragmentInformation["id"]] = [
                "id" => $sqFragmentInformation["id"],
                "name" => $sqFragmentInformation["name"],
                "quantity" => $totalSqFragments,
                "background" => $sqFragmentInformation["background"],
                "icon" => $sqFragmentInformation["icon"]
            ];

            $summonTicketInformation = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . 4001 . '?lang=en' ,cacheKey: 'material-' . 11);
            $rewards[4001] = [
                "id" => $summonTicketInformation["id"],
                "name" => $summonTicketInformation["name"],
                "quantity" => $totalSummonTicket + $requestPayload->getCurrentSummonTicket(),
                "background" => $summonTicketInformation["background"],
                "icon" => $summonTicketInformation["icon"]
            ];

            foreach($allEventsAndStory as $currentWar){
                if(isset($currentWar["warAdds"][0])) {
                    if($currentWar["warAdds"][0]["startedAt"] >= $startDateToTimeStamp && $currentWar["warAdds"][0]["startedAt"] <= $endDateToTimeStamp) {
                        foreach($currentWar["spots"] as $current_spot){
                            foreach($current_spot["quests"] as $current_quest) {
                                foreach($current_quest["gifts"] as $current_gift){
                                    if($current_gift["type"] === "item"){
                                        $giftInformation = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . $current_gift["objectId"] . '?lang=en' ,cacheKey: 'material-' . $current_gift["objectId"]);
                                        $rewards[$current_gift["objectId"]] = [
                                            "id" => $current_gift["objectId"],
                                            "name" => $giftInformation["name"],
                                            "quantity" => isset($rewards[$current_gift['objectId']]) ? $rewards[$current_gift["objectId"]]['quantity'] + $current_gift["num"] : $current_gift["num"],
                                            "background" => $giftInformation["background"],
                                            "icon" => $giftInformation["icon"]
                                        ];

                                    }
                                }

                                if(in_array("mainScenario", $currentWar["flags"])) {
                                    foreach($current_quest["phases"] as $currentPhase) {
                                        $rewards[$sqFragmentInformation["id"]]['quantity'] = $rewards[$sqFragmentInformation["id"]]['quantity'] + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $rewards[2]["quantity"] = $rewards[2]["quantity"] + $loginStreakSaintQuartzBonus + $requestPayload->getCurrentSaintQuartz();

            return new JsonResponse(
                data: $rewards,
                status: Response::HTTP_OK
            );

        } catch (ApiException|Exception $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
