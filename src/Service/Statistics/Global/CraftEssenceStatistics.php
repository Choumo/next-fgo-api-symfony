<?php

namespace App\Service\Statistics\Global;

class CraftEssenceStatistics
{
    /**
     * @var int
     */
    private int $fiveStar = 0;

    /**
     * @var int
     */
    private int $fourStar = 0;

    /**
     * @var int
     */
    private int $threeStar = 0;

    /**
     * @var int
     */
    private int $twoStar = 0;

    /**
     * @var int
     */
    private int $oneStar = 0;

    /**
     * @var array
     */
    private array $craftEssenceType = [];

    public function __construct()
    {
        $this->craftEssenceType = [
            "svtEquipFriendShip" => 0,
            "svtEquipCampaign" => 0,
            "svtEquipEvent" => 0,
            "svtEquipEventReward" => 0,
            "svtEquipManaExchange" => 0,
            "normal" => 0,
            "unknown" => 0
        ];
    }

    /**
     * @return int
     */
    public function getFiveStar(): int
    {
        return $this->fiveStar;
    }

    /**
     * @param int $fiveStar
     */
    public function setFiveStar(int $fiveStar): void
    {
        $this->fiveStar = $this->fiveStar + $fiveStar;
    }

    /**
     * @return int
     */
    public function getFourStar(): int
    {
        return $this->fourStar;
    }

    /**
     * @param int $fourStar
     */
    public function setFourStar(int $fourStar): void
    {
        $this->fourStar = $this->fourStar + $fourStar;
    }

    /**
     * @return int
     */
    public function getThreeStar(): int
    {
        return $this->threeStar;
    }

    /**
     * @param int $threeStar
     */
    public function setThreeStar(int $threeStar): void
    {
        $this->threeStar = $this->threeStar + $threeStar;
    }

    /**
     * @return int
     */
    public function getTwoStar(): int
    {
        return $this->twoStar;
    }

    /**
     * @param int $twoStar
     */
    public function setTwoStar(int $twoStar): void
    {
        $this->twoStar = $this->twoStar + $twoStar;
    }

    /**
     * @return int
     */
    public function getOneStar(): int
    {
        return $this->oneStar;
    }

    /**
     * @param int $oneStar
     */
    public function setOneStar(int $oneStar): void
    {
        $this->oneStar = $this->oneStar + $oneStar;
    }

    /**
     * @return array
     */
    public function getCraftEssenceType(): array
    {
        return $this->craftEssenceType;
    }

    /**
     * @param string $type
     * @param int $value
     */
    public function setCraftEssenceType(string $type, int $value): void
    {
        $this->craftEssenceType[$type] = $this->craftEssenceType[$type] + $value;
    }

    public function getTotalOfCraftEssence(): int
    {
        return $this->fiveStar + $this->fourStar + $this->threeStar + $this->twoStar + $this->oneStar;
    }

    public function getStatistics(): array
    {
        return [
            "totalGlobalCraftEssenceObtained" => $this->getTotalOfCraftEssence(),
            "totalGlobalFiveStarCraftEssenceObtained" => $this->getFiveStar(),
            "totalGlobalFourStarCraftEssenceObtained" => $this->getFourStar(),
            "totalGlobalThreeStarCraftEssenceObtained" => $this->getThreeStar(),
            "totalGlobalTwoStarCraftEssenceObtained" => $this->getTwoStar(),
            "totalGlobalOneStarCraftEssenceObtained" => $this->getOneStar(),
            "totalGlobalCraftEssenceObtainedByType" => $this->getCraftEssenceType(),
        ];
    }

}
