<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class UserResetPasswordPatchRequest extends AbstractRequest
{
    #[SerializedName('password')]
    #[Assert\PasswordStrength(['minScore' => Assert\PasswordStrength::STRENGTH_MEDIUM])]
    #[Assert\NotBlank]
    private string $password;

    #[SerializedName('token')]
    #[Assert\NotBlank]
    private string $token;

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }






}
