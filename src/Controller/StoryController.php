<?php

namespace App\Controller;

use App\Entity\Quest;
use App\Entity\User;
use App\Exception\ApiException;
use App\Exception\ApiValidationException;
use App\Factory\Request\ChapterRequest\ChapterPostRequest;
use App\Factory\Request\ServantRequest\ServantPatchRequest;
use App\Repository\QuestRepository;
use App\Repository\UserRepository;
use App\Service\Api;
use App\Service\JsonTransformer;
use App\Service\Statistics\Global\StoryStatistics;
use Doctrine\ORM\EntityManagerInterface;
use SensitiveParameter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/story')]
class StoryController extends AbstractController
{
    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}

    /**
     * Fonction qui permet de récupérer la liste des chapitres d'histoire
     *
     * @param Api $rayshift_api
     * @return JsonResponse,
     */
    #[Route('/list', name: 'app_story', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function storyList(Api $rayshift_api): JsonResponse
    {
        try{
            $storyResponse = [];

            $response = $rayshift_api->callRayshiftApi(endpoint: 'export/JP/nice_war_lang_en.json', cacheKey: 'war-list');
            $count_story = count($response);

            for($i = 0; $i < $count_story; $i++){
                if(!in_array(needle: "mainScenario", haystack: $response[$i]['flags']) || in_array(needle: $response[$i]['id'] , haystack: [300, 400, 401, 403, 14000, 13000, 12000, 11000])){
                    unset($response[$i]);
                }else{
                    if(in_array(needle: $response[$i]['id'] , haystack: [100, 101, 102, 103, 104, 105, 106, 107, 108])) {
                        $storyResponse[0][] = [
                            'id' => $response[$i]['id'],
                            'name' => $response[$i]['longName'],
                            'banner' => $response[$i]['banner'],
                        ];
                    }elseif (in_array(needle: $response[$i]['id'] , haystack: [201, 202, 203, 204])) {
                        $storyResponse[1][] = [
                            'id' => $response[$i]['id'],
                            'name' => $response[$i]['longName'],
                            'banner' => $response[$i]['banner'],
                        ];
                    }elseif (in_array(needle: $response[$i]['id'] , haystack: [301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311])) {
                        $storyResponse[2][] = [
                            'id' => $response[$i]['id'],
                            'name' => $response[$i]['longName'],
                            'banner' => $response[$i]['banner'],
                        ];
                    }elseif (in_array(needle: $response[$i]['id'] , haystack: [403])) {
                        $storyResponse[3][] = [
                            'id' => $response[$i]['id'],
                            'name' => $response[$i]['longName'],
                            'banner' => $response[$i]['banner'],
                        ];
                    }
                }
            }

            return new JsonResponse(
                data: $storyResponse,
                status: Response::HTTP_OK
            );
        } catch(\Exception $e) {
            return new JsonResponse(
                data: [
                    "status" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "message" => $e->getMessage()
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Fonction qui permet de récupérer la liste des matériaux pour un chapitre sélectionné
     *
     * @param int $id
     * @param Api $rayshift_api
     * @param QuestRepository $questRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route("/{id}/quests", name: "app_story_quest_list", methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function questsList(#[\SensitiveParameter] int $id, Api $rayshift_api, QuestRepository $questRepository, #[CurrentUser] User $user): JsonResponse
    {
        try {
            $response = $rayshift_api->callRayshiftApi(endpoint: 'nice/JP/war/'. $id .'?lang=en', cacheKey: "story-" . $id);
            $quest_array = [];

            if (isset($response['status_code'])) {
                return new JsonResponse($response, $response['status_code']);
            }

            $quest_array['id'] = $response['id'];
            $quest_array['name'] = $response['longName'];
            $quest_array['spots'] = [];
            $quest_array['rewards'] = [];

            foreach($response["spots"] as $current_spot){
                $quests = [];

                foreach($current_spot["quests"] as $current_quest) {
                    $quests[] = [
                        "quest_id" => $current_quest["id"],
                        "quest_name" => $current_quest["name"],
                    ];

                    foreach($current_quest["gifts"] as $current_gift){
                        if($current_gift["type"] === "item"){
                            $giftInformation = $rayshift_api->callRayshiftApi(endpoint: 'nice/JP/item/' . $current_gift["objectId"] . '?lang=en' ,cacheKey: 'material-' . $current_gift["objectId"]);
                            $quest_array['rewards'][$current_gift["objectId"]] = [
                                "id" => $current_gift["objectId"],
                                "name" => $giftInformation["name"],
                                "quantity" => isset($quest_array['rewards'][$current_gift['objectId']]) ? $quest_array['rewards'][$current_gift["objectId"]]['quantity'] + $current_gift["num"] : $current_gift["num"],
                                "background" => $giftInformation["background"],
                                "icon" => $giftInformation["icon"]
                            ];

                        }
                    }
                }


                $quest_array['spots'][] = [
                    "id" => $current_spot["id"],
                    "name" => $current_spot["name"],
                    "image" => $current_spot["image"],
                    "quests" => $quests,
                ];
            }

            $quest = $questRepository->findOneBy(['user' => $user, 'chapter_id' => $id]);

            if($quest === null) {
                $chapterState = "Not Done";
            } else {
                $chapterState = $quest->getChapterState();
            }

            $quest_array["state"] = $chapterState;

            return new JsonResponse($quest_array, 201);
        } catch(\Exception $e) {
            return new JsonResponse(["status" => "error", "message" => $e->getMessage()], 401);
        }
    }

    /**
     * Fonction qui permet à un utilisateur de mettre à jour l'état d'un chapitre
     *
     * @param string $chapter_id
     * @param EntityManagerInterface $entityManager
     * @param Api $rayshift_api
     * @param User|null $user
     * @param QuestRepository $questRepository
     * @param Request $request
     * @return JsonResponse
     */
    #[Route("/quest/update/{chapter_id}", name: "app_add_story_quest_list", methods: ["POST"])]
    #[IsGranted('ROLE_USER')]
    public function questAddToggle(#[SensitiveParameter] string $chapter_id, EntityManagerInterface $entityManager, Api $rayshift_api, #[CurrentUser] ?User $user, QuestRepository $questRepository, Request $request): JsonResponse
    {
        try {
            /** @var ChapterPostRequest $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: ChapterPostRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Impossible de désérialiser le contenu de la requête"
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $chapter = $questRepository->findOneBy(["chapter_id" => $chapter_id, "user" => $this->getUser()]);
            $response_chapter = $rayshift_api->callRayshiftApi(endpoint: "nice/JP/war/" .$chapter_id . "?lang=en", cacheKey: "chapter-" . $chapter_id);

            if(!$chapter) {
                $chapter = new Quest();
                $chapter->setChapterId($chapter_id);
                $chapter->setUser($user);
            }

            $chapter->setChapterState($requestPayload->getState());

            $entityManager->persist($chapter);
            $entityManager->flush();

            return new JsonResponse(data: [
                "status" => Response::HTTP_OK,
                "message" => "The state of the chapter has been updated"
            ], status: Response::HTTP_OK);
        } catch(\Exception $e) {
            return new JsonResponse(data: [
                "status" => "error",
                "message" => $e->getMessage()
            ], status: Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Fonction qui permet de récupérer les statistiques en lien avec les chapitres d'histoire pour l'utilisateur connecté
     *
     * @param User $user
     * @param QuestRepository $questRepository
     * @param Api $rayshiftApi
     * @return JsonResponse
     */
    #[Route("/statistics", name: "app_story_statistics", methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function userStatistics(#[CurrentUser] User $user, QuestRepository $questRepository, Api $rayshiftApi): JsonResponse
    {
        try {
            $questOfUser = $questRepository->findBy(["user" => $user]);
            $questOfApi = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_war_lang_en.json', cacheKey: 'war-list');

            $firstArcCleared = 0;
            $firstArcInProgress = 0;
            $firstArcNotDone = 0;

            $secondArcCleared = 0;
            $secondArcInProgress = 0;
            $secondArcNotDone = 0;

            $thirdArcCleared = 0;
            $thirdArcInProgress = 0;
            $thirdArcNotDone = 0;

            $fourthArcCleared = 0;
            $fourthArcInProgress = 0;
            $fourthArcNotDone = 0;

            $nbChapters = 0;
            $nbQuests = 0;

            foreach ($questOfApi as $currentQuest) {
                if(!in_array(needle: "mainScenario", haystack: $currentQuest['flags']) || in_array(needle: $currentQuest['id'] , haystack: [300, 400, 401, 403, 14000, 13000, 12000, 11000])) {
                    continue;
                } else {
                    $userQuest = $questRepository->findOneBy(["user" => $user, "chapter_id" => $currentQuest['id']]);
                    $nbChapters = $nbChapters + 1;

                    foreach ($currentQuest["spots"] as $currentQuestSpot) {
                        $nbQuests = $nbQuests + count($currentQuestSpot["quests"]);
                    }

                    if(in_array(needle: $currentQuest['id'] , haystack: [100, 101, 102, 103, 104, 105, 106, 107, 108])) {
                        if($userQuest !== null) {
                            switch ($userQuest->getChapterState()) {
                                case "In Progress":
                                    $firstArcInProgress = $firstArcInProgress + 1;
                                    break;
                                case "Cleared":
                                    $firstArcCleared = $firstArcCleared + 1;
                                    break;
                                default:
                                    $firstArcNotDone = $firstArcNotDone + 1;
                                    break;
                            }
                        } else {
                            $firstArcNotDone = $firstArcNotDone + 1;
                        }
                    }elseif (in_array(needle: $currentQuest['id'] , haystack: [201, 202, 203, 204])) {
                        if($userQuest !== null) {
                            switch ($userQuest->getChapterState()) {
                                case "In Progress":
                                    $secondArcInProgress = $secondArcInProgress + 1;
                                    break;
                                case "Cleared":
                                    $secondArcCleared = $secondArcCleared + 1;
                                    break;
                                default:
                                    $secondArcNotDone = $secondArcNotDone + 1;
                                    break;
                            }
                        } else {
                            $secondArcNotDone = $secondArcNotDone + 1;
                        }
                    }elseif (in_array(needle: $currentQuest['id'] , haystack: [301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311])) {
                        if($userQuest !== null) {
                            switch ($userQuest->getChapterState()) {
                                case "In Progress":
                                    $thirdArcInProgress = $thirdArcInProgress + 1;
                                    break;
                                case "Cleared":
                                    $thirdArcCleared = $thirdArcCleared + 1;
                                    break;
                                default:
                                    $thirdArcNotDone = $thirdArcNotDone + 1;
                                    break;
                            }
                        } else {
                            $thirdArcNotDone = $thirdArcNotDone + 1;
                        }
                    }elseif (in_array(needle: $currentQuest['id'] , haystack: [403])) {
                        if($userQuest !== null) {
                            switch ($userQuest->getChapterState()) {
                                case "In Progress":
                                    $fourthArcInProgress = $fourthArcInProgress + 1;
                                    break;
                                case "Cleared":
                                    $fourthArcCleared = $fourthArcCleared + 1;
                                    break;
                                default:
                                    $fourthArcNotDone = $fourthArcNotDone + 1;
                                    break;
                            }
                        } else {
                            $fourthArcNotDone = $fourthArcNotDone + 1;
                        }
                    }
                }
            }

            $userStatistics = [
                "totalChaptersCleared" => $firstArcCleared + $secondArcCleared + $thirdArcCleared + $fourthArcCleared,
                "totalQuests" => $nbQuests,
                "totalChapters" => $nbChapters,
                "totalChaptersClearedPercentage" => round((($firstArcCleared + $secondArcCleared + $thirdArcCleared + $fourthArcCleared) / $nbChapters * 100)),
                "chapters" => [
                    0 => [
                        "Cleared" => $firstArcCleared,
                        "InProgress" => $firstArcInProgress,
                        "NotDone" => $firstArcNotDone,
                    ],
                    1 => [
                        "Cleared" => $secondArcCleared,
                        "InProgress" => $secondArcInProgress,
                        "NotDone" => $secondArcNotDone,
                    ],
                    2 => [
                        "Cleared" => $thirdArcCleared,
                        "InProgress" => $thirdArcInProgress,
                        "NotDone" => $thirdArcNotDone,
                    ],
                    3 => [
                        "Cleared" => $fourthArcCleared,
                        "InProgress" => $fourthArcInProgress,
                        "NotDone" => $fourthArcNotDone,
                    ],
                ]
            ];

            return new JsonResponse(
                data: $userStatistics,
                status: Response::HTTP_OK
            );

        } catch (ApiException $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de récupérer les statistiques globales de tout les utilisateur pour les chapitres d'histoire
     *
     * @param UserRepository $userRepository
     * @param Api $rayshiftApi
     * @param QuestRepository $questRepository
     * @return JsonResponse
     */
    #[Route('/global-statistics', name: 'app_story_global-statistics', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function globalStatistics(UserRepository $userRepository, Api $rayshiftApi, QuestRepository $questRepository) : JsonResponse
    {
        try {
            $users = $userRepository->findAll();
            $questOfApi = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_war_lang_en.json', cacheKey: 'war-list');
            $storyStatistics = new StoryStatistics();

            foreach ($users as $user) {
                if($user->isShareData() === true) {
                    $questOfUser = $user->getQuests();
                    foreach ($questOfApi as $currentQuest) {
                        if(!in_array(needle: "mainScenario", haystack: $currentQuest['flags']) || in_array(needle: $currentQuest['id'] , haystack: [300, 400, 401, 403, 14000, 13000, 12000, 11000])) {
                            continue;
                        } else {
                            $userQuest = $questRepository->findOneBy(["user" => $user, "chapter_id" => $currentQuest['id']]);
                            if(in_array(needle: $currentQuest['id'] , haystack: [100, 101, 102, 103, 104, 105, 106, 107, 108])) {
                                $storyStatistics->setFirstArc($userQuest?->getChapterState());
                            }elseif (in_array(needle: $currentQuest['id'] , haystack: [201, 202, 203, 204])) {
                                $storyStatistics->setSecondArc($userQuest?->getChapterState());
                            }elseif (in_array(needle: $currentQuest['id'] , haystack: [301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311])) {
                                $storyStatistics->setThirdArc($userQuest?->getChapterState());
                            }elseif (in_array(needle: $currentQuest['id'] , haystack: [403])) {
                                $storyStatistics->setFourthArc($userQuest?->getChapterState());
                            }

                        }
                    }
                }
            }

            return new JsonResponse(
                data: $storyStatistics->getStatistics(),
                status: Response::HTTP_OK
            );
        } catch (ApiException|\Exception $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
