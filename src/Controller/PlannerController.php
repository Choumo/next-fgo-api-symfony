<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\ApiException;
use App\Exception\ApiValidationException;
use App\Factory\Request\ManagerRequest\ManagerPostRequest;
use App\Factory\Request\Planner\PlannerPostRequest;
use App\Repository\ServantRepository;
use App\Service\Api;
use App\Service\JsonTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/planner')]
class PlannerController extends AbstractController
{
    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}

    /**
     * Fonction qui permet de récupérer la liste des servants
     * @param Api $rayshiftApi
     * @param ServantRepository $servantRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/servant-list', name: 'app_planner_getservantinformationplanner', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function getServantInformationPlanner(Api $rayshiftApi, ServantRepository $servantRepository, #[CurrentUser] User $user): JsonResponse
    {
        try {
            $servant_array = [];

            $response = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_servant_lang_en.json', cacheKey: 'servant-list');

            $count_servants = count($response);

            for ($i = 0; $i < $count_servants; $i++) {
                if ($response[$i]['type'] === 'enemyCollectionDetail') {
                    continue;
                } else {
                    $userHasServant = $servantRepository->findOneBy(["servant_id" => $response[$i]['collectionNo'], "user" => $user]);
                    if($userHasServant) {
                        $servant_array[] = [
                            'id' => $response[$i]['collectionNo'],
                            'name' => $response[$i]['name'],
                            'class' => $response[$i]['className'],
                            'rarity' => $response[$i]['rarity'],
                            'graph' => $response[$i]['extraAssets']['charaGraph']['ascension'][4],
                            'icon' => $response[$i]['extraAssets']['faces']['ascension'][4],
                            'ascension' => 0,
                            'skills' => [
                                'skill1' => $userHasServant->getServantLevelSkill1(),
                                'skill2' => $userHasServant->getServantLevelSkill2(),
                                'skill3' => $userHasServant->getServantLevelSkill3(),
                            ],
                            'appends' => [
                                'append1' => $userHasServant->getServantLevelAppend1(),
                                'append2' => $userHasServant->getServantLevelAppend2(),
                                'append3' => $userHasServant->getServantLevelAppend3(),
                            ]
                        ];
                    } else {
                        $servant_array[] = [
                            'id' => $response[$i]['collectionNo'],
                            'name' => $response[$i]['name'],
                            'class' => $response[$i]['className'],
                            'rarity' => $response[$i]['rarity'],
                            'graph' => $response[$i]['extraAssets']['charaGraph']['ascension'][4],
                            'icon' => $response[$i]['extraAssets']['faces']['ascension'][4],
                            'ascension' => 0,
                            'skills' => [
                                'skill1' => 1,
                                'skill2' => 1,
                                'skill3' => 1,
                            ],
                            'appends' => [
                                'append1' => 1,
                                'append2' => 1,
                                'append3' => 1,
                            ]
                        ];
                    }

                }
            }

            return new JsonResponse(
                data: $servant_array,
                status: Response::HTTP_OK
            );
        } catch (ApiException $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de récupérer la liste des ressources pour les servants sélectionné par l'utilisateur
     *
     * @param Request $request
     * @param Api $rayshiftApi
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/servant-ressources', name: 'app_planner_getservantressources', methods: ["POST"])]
    #[IsGranted('ROLE_USER')]
    public function getServantRessources(Request $request, Api $rayshiftApi, #[CurrentUser] User $user): JsonResponse
    {
        try {
            /** @var ManagerPostRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: 'App\Factory\Request\ManagerRequest\ManagerPostRequest[]');
            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                foreach ($requestPayload as $payloadServant) {
                    $payload = $payloadServant->validate($this->validator);
                }
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $neededRessources = [];

            /** @var ManagerPostRequest|null $servant */
            foreach ($requestPayload as $servant) {
                $apiServantData = $rayshiftApi->callRayshiftApi(endpoint:'nice/JP/servant/' . $servant->getId() . '?lang=en', cacheKey: 'servant-'.$servant->getId());

                $neededRessources['servants'][$servant->getId()] = [
                    'id' => $servant->getId(),
                    'name' => $servant->getName(),
                    'class' => $servant->getClass(),
                    'graph' => $servant->getGraph(),
                    'icon' => $servant->getIcon(),
                    'rarity' => $servant->getRarity(),
                    'ressources' => [],
                ];

                foreach ($servant->getSkills() as $servantSkillKey => $servantSkillValue) {
                    foreach ($apiServantData['skillMaterials'] as $key => $skillLvl) {
                        if (($key >= ($servant->getSkills()->get($servantSkillKey) - 1) && $key <= ($servant->getTarget()->get($servantSkillKey) - 1))) {
                            foreach ($skillLvl['items'] as $currentMaterial) {
                                $apiMaterialData = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . $currentMaterial['item']['id'] . '?lang=en', cacheKey: 'item-' . $currentMaterial['item']['id']);

                                if(!isset($neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]) && !isset($neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]["quantity"])) {
                                    $neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]["quantity"] = 0;
                                }

                                $neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']] = [
                                    "id" => $apiMaterialData["id"],
                                    "name" => $apiMaterialData["name"],
                                    "background" => $apiMaterialData["background"],
                                    "icon" => $apiMaterialData["icon"],
                                    "quantity" => $neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]["quantity"] + $currentMaterial['amount']
                                ];
                            }
                        }
                    }
                }


                foreach ($servant->getAppends() as $servantAppendKey => $servantAppendValue) {
                    foreach ($apiServantData['appendSkillMaterials'] as $key => $skillLvl) {
                        if (($key >= ($servant->getAppends()->get($servantAppendKey) - 1) && $key <= ($servant->getTarget()->get($servantAppendKey) - 1))) {
                            foreach ($skillLvl['items'] as $currentMaterial) {
                                $apiMaterialData = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . $currentMaterial['item']['id'] . '?lang=en', cacheKey: 'item-' . $currentMaterial['item']['id']);

                                if(!isset($neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]) && !isset($neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]["quantity"])) {
                                    $neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]["quantity"] = 0;
                                }

                                $neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']] = [
                                    "id" => $apiMaterialData["id"],
                                    "name" => $apiMaterialData["name"],
                                    "background" => $apiMaterialData["background"],
                                    "icon" => $apiMaterialData["icon"],
                                    "quantity" => $neededRessources['servants'][$servant->getId()]['ressources'][$apiMaterialData['id']]["quantity"] + $currentMaterial['amount']
                                ];
                            }
                        }
                    }
                }
            }

            $totalNeededRessrouces = [];
            foreach ($neededRessources['servants'] as $servant) {
                foreach ($servant['ressources'] as $ressource) {
                    if(!isset($totalNeededRessrouces[$ressource['id']]) && !isset($totalNeededRessrouces[$ressource['id']]['quantity'])) {
                        $totalNeededRessrouces[$ressource['id']]['quantity'] = 0;
                    }

                    $totalNeededRessrouces[$ressource['id']] = [
                        "id" => $ressource["id"],
                        "name" => $ressource["name"],
                        "background" => $ressource["background"],
                        "icon" => $ressource["icon"],
                        "quantity" => $totalNeededRessrouces[$ressource['id']]['quantity'] + $ressource['quantity']
                    ];
                }
            }

            $userRessources = $user->getMaterials();
            foreach ($userRessources as $userRessource) {
                $apiMaterialData = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . $userRessource->getMaterialId() . '?lang=en', cacheKey: 'item-' . $userRessource->getMaterialId());

                $neededRessources['user'][$userRessource->getMaterialId()] = [
                    "id" => $apiMaterialData["id"],
                    "name" => $apiMaterialData["name"],
                    "background" => $apiMaterialData["background"],
                    "icon" => $apiMaterialData["icon"],
                    "quantity" => $userRessource->getMaterialQuantity()
                ];

                if(isset($totalNeededRessrouces[$userRessource->getMaterialId()])) {
                    $totalNeededRessrouces[$userRessource->getMaterialId()]['quantity'] = $totalNeededRessrouces[$userRessource->getMaterialId()]['quantity'] - $userRessource->getMaterialQuantity();
                }
            }

            $neededRessources['total'] = $totalNeededRessrouces;

            return new JsonResponse(
                data: $neededRessources,
                status: Response::HTTP_OK
            );
        } catch (\Exception|ApiException $exception) {
            return new JsonResponse(
                data: [
                    "message" => [
                        $exception->getMessage(),
                        $exception->getLine()
                    ],
                    "line" => $exception->getLine()
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
