<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/middleware')]
class MiddlewareController extends AbstractController
{
    /**
     * Fonction qui permet de vérifier que l'utilisateur est connecté et qu'il existe bien dans la base de données
     *
     * @return JsonResponse
     */
    #[Route('/', name: 'middleware_index', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function index(): JsonResponse
    {
        return new JsonResponse(status: Response::HTTP_OK);
    }
}
