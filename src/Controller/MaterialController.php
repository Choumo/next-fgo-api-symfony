<?php

namespace App\Controller;

use App\Entity\Material;
use App\Entity\User;
use App\Exception\ApiException;
use App\Exception\ApiValidationException;
use App\Factory\Request\MaterialRequest\MaterialPatchRequest;
use App\Repository\MaterialRepository;
use App\Service\Api;
use App\Service\JsonTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/material')]
class MaterialController extends AbstractController
{

    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}

    /**
     * Fonction qui permet de récupérer la liste des matériaux et leurs quantités pour l'utilisateur connecté
     *
     * @param Api $rayshiftApi
     * @param MaterialRepository $materialRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/list', name: 'app_material_list')]
    #[IsGranted('ROLE_USER')]
    public function index(Api $rayshiftApi, MaterialRepository $materialRepository, #[CurrentUser] User $user): JsonResponse
    {
        try {
            $material_array = [];
            $materialApiList = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_item_lang_en.json', cacheKey: 'material-list');
            $countMaterials = count($materialApiList);

            foreach ($materialApiList as $currentMaterial) {
                if(!in_array($currentMaterial["type"], ["qp", "stone", "skillLvUp", "pp", "rarePri", "stoneFragments", "apRecover", "gachaTicket"])){
                    continue;
                }else{
                    $userQuantity = $materialRepository->findOneBy(["material_id" => $currentMaterial["id"], "material_user" => $user]);

                    if($userQuantity === null) {
                        $quantity = 0;
                    } else {
                        $quantity = $userQuantity->getMaterialQuantity();
                    }

                    $material_array[] = [
                        "id" => $currentMaterial["id"],
                        "name" => $currentMaterial["name"],
                        "background" => $currentMaterial["background"],
                        "icon" => $currentMaterial["icon"],
                        "quantity" => $quantity
                    ];
                }
            }

            return new JsonResponse(
                data: $material_array,
                status: Response::HTTP_OK
            );

        } catch (ApiException $exception) {
            return new JsonResponse(
                data: [
                    "status" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "message" => $exception->getMessage()
                ], status: Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Fonction qui permet de mettre à jour les informations d'une ressource pour un utilisateur
     *
     * @param Api $rayshiftApi
     * @param EntityManagerInterface $entityManager
     * @param MaterialRepository $materialRepository
     * @param User $user
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/update', name: 'app_material_update', methods: ["PATCH"])]
    #[IsGranted('ROLE_USER')]
    public function update(Api $rayshiftApi, EntityManagerInterface $entityManager, MaterialRepository $materialRepository, #[CurrentUser] User $user, Request $request): JsonResponse
    {
        try {
            /** @var MaterialPatchRequest $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: MaterialPatchRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Impossible de désérialiser le contenu de la requête"
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . $requestPayload->getId() . '?lang=en', cacheKey: 'item-' . $requestPayload->getId());

            $userMaterial = $materialRepository->findOneBy(["material_user" => $user, "id" => $requestPayload->getId(), "material_id" => $requestPayload->getId()]);

            if($userMaterial) {
                $userMaterial->setMaterialQuantity($requestPayload->getQuantity());
            } else {
                $userMaterial = new Material();
                $userMaterial->setMaterialId($requestPayload->getId());
                $userMaterial->setMaterialUser($user);
                $userMaterial->setMaterialQuantity($requestPayload->getQuantity());
            }

            $entityManager->persist($userMaterial);
            $entityManager->flush();

            return new JsonResponse(
                data: [
                    "status" => Response::HTTP_OK,
                    "message" => "Material quantity updated"
                ],
                status: Response::HTTP_OK
            );

        } catch (ApiException $exception) {
            return new JsonResponse(
                data: [
                    "status" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "message" => $exception->getMessage()
                ], status: Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
