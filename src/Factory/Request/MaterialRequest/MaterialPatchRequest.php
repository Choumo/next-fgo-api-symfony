<?php

namespace App\Factory\Request\MaterialRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class MaterialPatchRequest extends AbstractRequest
{
    #[SerializedName('id')]
    #[Assert\NotBlank]
    private int $id;
    #[SerializedName('quantity')]
    #[Assert\NotBlank]
    private int $quantity;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }


}
