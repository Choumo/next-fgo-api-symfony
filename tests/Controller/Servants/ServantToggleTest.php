<?php

namespace App\Tests\Controller\Servants;

use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServantToggleTest extends WebTestCase
{
    /**
     * Cette fonction permet de tester qu'il n'est pas possible d'ajouter ou supprimer un servant si l'utilisateur n'est pas connecté
     *
     * Doit retourner un code 401
     * @return void
     */
    public function testToggleServantUserNotLogged() : void
    {

        $client = static::createClient();

        $response = $client->request("GET", "/servants/add/101", []);

        $this->assertResponseStatusCodeSame(401);
    }

    /**
     * Cette fonction permet de tester l'ajout ou la suppression d'un servant qui existe dans l'API
     *
     * Doit retourner un code 200
     * @return void
     * @throws Exception
     */
    public function testToggleServantUserLogged() : void
    {

        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $response = $client->request("GET", "/servants/add/101", []);

        $this->assertResponseIsSuccessful();
    }

    /**
     * Cette fonction permet de tester l'ajouter ou la suppression d'un servant qui n'existe pas dans l'API
     *
     * Doit retourner un code 422
     * @return void
     * @throws Exception
     */
    public function testToggleServantUserLoggedButServantDoesNotExistsInApi() : void
    {

        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $response = $client->request("GET", "/servants/add/100000000", []);

        $this->assertResponseStatusCodeSame(422);
    }
}
