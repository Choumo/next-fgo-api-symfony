<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Assert\Email()]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var ?string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Servant::class, orphanRemoval: true)]
    private Collection $servants;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: CraftEssence::class, orphanRemoval: true)]
    private Collection $craftEssences;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Quest::class, orphanRemoval: true)]
    private Collection $quests;

    #[ORM\OneToMany(mappedBy: 'material_user', targetEntity: Material::class, orphanRemoval: true)]
    private Collection $materials;

    #[ORM\Column]
    private ?bool $shareData = false;

    #[ORM\Column(nullable: true)]
    private ?bool $is2Fa = false;

    public function __construct()
    {
        $this->servants = new ArrayCollection();
        $this->craftEssences = new ArrayCollection();
        $this->quests = new ArrayCollection();
        $this->materials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Servant>
     */
    public function getServants(): Collection
    {
        return $this->servants;
    }

    public function addServant(Servant $servant): static
    {
        if (!$this->servants->contains($servant)) {
            $this->servants->add($servant);
            $servant->setUser($this);
        }

        return $this;
    }

    public function removeServant(Servant $servant): static
    {
        if ($this->servants->removeElement($servant)) {
            // set the owning side to null (unless already changed)
            if ($servant->getUser() === $this) {
                $servant->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CraftEssence>
     */
    public function getCraftEssences(): Collection
    {
        return $this->craftEssences;
    }

    public function addCraftEssence(CraftEssence $craftEssence): static
    {
        if (!$this->craftEssences->contains($craftEssence)) {
            $this->craftEssences->add($craftEssence);
            $craftEssence->setUser($this);
        }

        return $this;
    }

    public function removeCraftEssence(CraftEssence $craftEssence): static
    {
        if ($this->craftEssences->removeElement($craftEssence)) {
            // set the owning side to null (unless already changed)
            if ($craftEssence->getUser() === $this) {
                $craftEssence->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Quest>
     */
    public function getQuests(): Collection
    {
        return $this->quests;
    }

    public function addQuest(Quest $quest): static
    {
        if (!$this->quests->contains($quest)) {
            $this->quests->add($quest);
            $quest->setUser($this);
        }

        return $this;
    }

    public function removeQuest(Quest $quest): static
    {
        if ($this->quests->removeElement($quest)) {
            // set the owning side to null (unless already changed)
            if ($quest->getUser() === $this) {
                $quest->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Material>
     */
    public function getMaterials(): Collection
    {
        return $this->materials;
    }

    public function addMaterial(Material $material): static
    {
        if (!$this->materials->contains($material)) {
            $this->materials->add($material);
            $material->setMaterialUser($this);
        }

        return $this;
    }

    public function removeMaterial(Material $material): static
    {
        if ($this->materials->removeElement($material)) {
            // set the owning side to null (unless already changed)
            if ($material->getMaterialUser() === $this) {
                $material->setMaterialUser(null);
            }
        }

        return $this;
    }

    public function isShareData(): ?bool
    {
        return $this->shareData;
    }

    public function setShareData(bool $shareData): static
    {
        $this->shareData = $shareData;

        return $this;
    }

    public function isIs2Fa(): ?bool
    {
        return $this->is2Fa;
    }

    public function setIs2Fa(bool $is2Fa): static
    {
        $this->is2Fa = $is2Fa;

        return $this;
    }
}
