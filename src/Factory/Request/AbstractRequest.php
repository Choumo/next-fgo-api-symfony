<?php

namespace App\Factory\Request;

use App\Exception\ApiValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractRequest
{
    public function validate(ValidatorInterface $validator): static
    {
        $validationErrors = $validator->validate($this);

        if(count($validationErrors) > 0) {
            throw new ApiValidationException($validationErrors);
        }

        return $this;
    }
}
