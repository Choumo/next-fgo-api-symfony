<?php

namespace App\Tests\Controller\Quests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class QuestsTest extends WebTestCase
{
    public function testShowListOfQuestIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $client->loginUser($user);
        $response = $client->request('GET', '/story/list');

        $this->assertResponseIsSuccessful();
    }
 
    public function testShowListOfQuestIfUserNotLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $response = $client->request('GET', '/story/list');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testShowChapterInformationIfUserLogged(): void
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $client->loginUser($user);
        $response = $client->request('GET', '/story/104/quests');

        $this->assertResponseIsSuccessful();
    }
 
    public function testShowChapterInformationIfUserNotLogged(): void
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $response = $client->request('GET', '/story/104/quests');

        $this->assertResponseStatusCodeSame(401);
    }

    
    public function testShowChapterInformationIfChapterDontExistInApi(): void
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $response = $client->request('GET', '/story/10456487484/quests');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testAddOrRemoveQuestToUserDatabaseIfUserLogged(): void 
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $client->loginUser($user);
        $response = $client->request('POST', '/story/quest/add/104/1000403');

        $this->assertResponseIsSuccessful();
    }

    public function testAddOrRemoveQuestToUserDatabaseIfUserNotLogged(): void 
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $response = $client->request('POST', '/story/quest/add/104/1000403');

        $this->assertResponseStatusCodeSame(401);
    }
    
    public function testAddOrRemoveQuestToUserDatabaseIfChapterDontExistInApi(): void 
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $client->loginUser($user);
        $response = $client->request('POST', '/story/quest/add/1044445897/1000403');

        $this->assertResponseStatusCodeSame(401);
    }


    public function testAddOrRemoveQuestToUserDatabaseIfQuestDontExistInApi(): void 
    {

        $client = static::createClient();
        $container = static::getContainer();
        
        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);
        
        $client->loginUser($user);
        $response = $client->request('POST', '/story/quest/add/104/10944458746644700403');

        $this->assertResponseStatusCodeSame(401);
    }
}
