<?php

namespace App\Repository;

use App\Entity\EmailCodeAuthentication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EmailCodeAuthentication>
 *
 * @method EmailCodeAuthentication|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailCodeAuthentication|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailCodeAuthentication[]    findAll()
 * @method EmailCodeAuthentication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailCodeAuthenticationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailCodeAuthentication::class);
    }

//    /**
//     * @return EmailCodeAuthentication[] Returns an array of EmailCodeAuthentication objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EmailCodeAuthentication
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
