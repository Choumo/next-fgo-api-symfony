<?php

namespace App\Tests\Controller\Servants;

use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServantApiListTest extends WebTestCase
{

    /**
     * Cette fonction permet de tester la récupération de la liste des servants si l'utilisateur est connecté
     *
     * Doit retourner un code 200
     * @return void
     * @throws Exception
     */
    public function testGetServantsFromApiIfUserLogged() : void
    {

        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $response = $client->request("GET", "/servants/list", []);

        $this->assertResponseIsSuccessful();
    }

    /**
     * Cette fonction permet de tester la récupération de la liste des servants si l'utilisateur n'est pas connecté
     *
     * Doit retourner un code 401
     * @return void
     * @throws Exception
     */
    public function testGetServantsFromApiIfUserNotLogged() : void
    {
        $client = static::createClient();

        $response = $client->request("GET", "/servants/list");

        $this->assertResponseStatusCodeSame(401);
    }
}
