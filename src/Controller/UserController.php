<?php

namespace App\Controller;

use App\Entity\EmailCodeAuthentication;
use App\Entity\PasswordReset;
use App\Entity\User;
use App\Exception\ApiValidationException;
use App\Factory\Request\UserRequest\User2FaLoginRequest;
use App\Factory\Request\UserRequest\User2LoginFaRequest;
use App\Factory\Request\UserRequest\UserRegisterRequest;
use App\Factory\Request\UserRequest\UserResetPasswordPatchRequest;
use App\Factory\Request\UserRequest\UserResetPasswordPostRequest;
use App\Factory\Request\UserRequest\UserShareDataRequest;
use App\Repository\EmailCodeAuthenticationRepository;
use App\Repository\PasswordResetRepository;
use App\Repository\UserRepository;
use App\Service\JsonTransformer;
use App\Service\Token;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TheSeer\Tokenizer\Exception;

#[Route('/api/user', name: 'api_')]
class UserController extends AbstractController
{
    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}

    /**
     * Fonction qui permet à un utilisateur de s'enregistrer
     *
     * @param EntityManagerInterface $em
     * @param UserPasswordHasherInterface $passwordHasher
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/register', name: 'app_user_register', methods: ['POST'])]
    public function userSignUp(EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher, Request $request): JsonResponse
    {
        try {
            /** @var UserRegisterRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: UserRegisterRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $user = new User();

            $hashedPassword = $passwordHasher->hashPassword(user: $user, plainPassword: $requestPayload->getPassword());

            $user->setEmail($requestPayload->getEmail());
            $user->setPassword($hashedPassword);

            $em->persist($user);
            $em->flush();


            return $this->json(
                data: [
                    'status' => 'success',
                    'message' => 'Your account has been created',
                ], status: Response::HTTP_CREATED);

        } catch (\Exception|\Throwable $exception) {
            return new JsonResponse(
                data: "An account already exists with this address email",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet à un utilisateur de se connecter
     *
     * @param User|null $user
     * @param Token $tokenService
     * @param Request $request
     * @param EmailCodeAuthenticationRepository $emailCodeAuthenticationRepository
     * @param MailerInterface $mailer
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route(path: '/login', name: 'login', methods: ['POST'])]
    public function login(#[CurrentUser] ?User $user, Token $tokenService, Request $request, EmailCodeAuthenticationRepository $emailCodeAuthenticationRepository, MailerInterface $mailer, EntityManagerInterface $entityManager): Response
    {
        if (null === $user) {
            return $this->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'payload' => [
                    'message' => 'Identifiants invalides',
                ]
            ], Response::HTTP_UNAUTHORIZED);
        }

        $jwt = $tokenService->generateJWT($user->getId());

        if($user->isIs2Fa() === true) {
            try {

                $userEmailCode = $emailCodeAuthenticationRepository->findOneBy(['UserOwner' => $user]);

                if($userEmailCode == null) {
                    $userEmailCode = new EmailCodeAuthentication();
                }

                $uniqueIdentifier = $tokenService->generatePasswordResetToken($user->getId());
                $urlEmailCode = "http://localhost:3000/multi-factor-authentication?id=" . $uniqueIdentifier;
                $code = bin2hex(random_bytes(20));

                $email = (new Email())
                    ->from('verify@next-fgo-manager.app')
                    //->to($user->getEmail())
                    ->to("alexandre-petit33@proton.me")
                    ->subject('Time for Symfony Mailer!')
                    ->text('Sending emails is fun again!')
                    ->html('<p>Please use this code : "'. $code .'"</p><p>You can access the login page here : <a href="'. $urlEmailCode .'">'. $urlEmailCode .'</a></p>');

                $mailer->send($email);

                $userEmailCode->setToken($code);
                $date = new \DateTimeImmutable('now');
                $userEmailCode->setCreatedAt($date);
                $userEmailCode->setUserOwner($user);
                $userEmailCode->setUniqueIdentifier($uniqueIdentifier);

                $entityManager->persist($userEmailCode);
                $entityManager->flush();

                return $this->json([
                    'status' => Response::HTTP_OK,
                    'payload' => [
                        'login' => 'success',
                        'require_2fa' => true,
                        'id' => $uniqueIdentifier
                    ]
                ], Response::HTTP_OK);
            } catch (Exception $exception) {

                return new JsonResponse(
                    data: $exception->getMessage(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
        } else {
            return $this->json([
                'status' => Response::HTTP_OK,
                'payload' => [
                    'login' => 'success',
                    'require_2fa' => false,
                    'token' => $jwt,
                ],
            ], Response::HTTP_OK);
        }
    }

    /**
     * Fonction qui permet à un utilisateur de démarrer le processus de reset du mot de passe
     *
     * @param MailerInterface $mailer
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @param Request $request
     * @param Token $tokenService
     * @param PasswordResetRepository $passwordResetRepository
     * @return JsonResponse
     */
    #[Route('/reset-password', name: 'reset_password', methods: ['POST'])]
    public function sendMailResetPassword(MailerInterface $mailer, EntityManagerInterface $entityManager, UserRepository $userRepository, Request $request, Token $tokenService, PasswordResetRepository $passwordResetRepository) : JsonResponse {
        try {
            /** @var UserResetPasswordPostRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: UserResetPasswordPostRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $user = $userRepository->findOneBy(['email' => $requestPayload->getEmail()]);

            if($user !== null) {

                $userPasswordToken = $passwordResetRepository->findOneBy(['UserOwner' => $user]);

                if($userPasswordToken === null) {
                    $userPasswordToken = new PasswordReset();
                }

                $token = $tokenService->generatePasswordResetToken($user->getId());

                $urlResetPassword = "http://localhost:3000/password-reset/" . $token;

                $email = (new Email())
                    ->from('reset-password@next-fgo-manager.app')
                    //->to($user->getEmail())
                    ->to("alexandre-petit33@proton.me")
                    ->subject('Time for Symfony Mailer!')
                    ->text('Sending emails is fun again!')
                    ->html('<p>Link to reset your password : <a href="'. $urlResetPassword .'"></a>'. $urlResetPassword  .'</p>');

                $mailer->send($email);

                $userPasswordToken->setToken($token);
                $date = new \DateTimeImmutable('now');
                $userPasswordToken->setCreatedAt($date);
                $userPasswordToken->setUserOwner($user);

                $entityManager->persist($userPasswordToken);
                $entityManager->flush();
            }

            return $this->json(
                data: [
                    'status' => 'success',
                    'message' => 'You will soon receive the link to reset your password in an email.',
                ], status: Response::HTTP_OK);

        } catch (\Exception|Exception $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de reset son mot de passe
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param PasswordResetRepository $passwordResetRepository
     * @param UserPasswordHasherInterface $passwordHasher
     * @return JsonResponse
     */
    #[Route('/reset-password', name: 'reset_password_patch', methods: ['PATCH'])]
    public function forgotPasswordReset(Request $request, EntityManagerInterface $entityManager, PasswordResetRepository $passwordResetRepository, UserPasswordHasherInterface $passwordHasher) : JsonResponse {
        try {
            /** @var UserResetPasswordPatchRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: UserResetPasswordPatchRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $passwordReset = $passwordResetRepository->findOneBy(['token' => $requestPayload->getToken()]);

            if($passwordReset === null) {
                throw new \Exception("The account does not exist");
            }

            if((time() - strtotime($passwordReset->getCreatedAt()->format('Y-m-d H:i:s'))) > 600) {
                throw new \Exception("The link has expired");
            }

            $user = $passwordReset->getUserOwner();

            $hashedPassword = $passwordHasher->hashPassword(user: $user, plainPassword: $requestPayload->getPassword());
            $user->setPassword($hashedPassword);

            $entityManager->persist($user);
            $entityManager->remove($passwordReset);
            $entityManager->flush();

            return $this->json(
                data: [
                    'status' => 'success',
                    'message' => 'You will soon receive the link to reset your password in an email.',
                ], status: Response::HTTP_OK);

        } catch (\Exception|Exception $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }


    /**
     * Fonction qui permet de se connecter avec le 2 factor authentication
     *
     * @param Request $request
     * @param Token $tokenService
     * @param EntityManagerInterface $entityManager
     * @param EmailCodeAuthenticationRepository $emailCodeAuthenticationRepository
     * @param UserPasswordHasherInterface $passwordHasher
     * @return JsonResponse
     */
    #[Route('/two-factor-login', name: 'two_factor_login', methods: ['POST'])]
    public function multiFaAuthenticate(Request $request, Token $tokenService, EntityManagerInterface $entityManager, EmailCodeAuthenticationRepository $emailCodeAuthenticationRepository, UserPasswordHasherInterface $passwordHasher) : JsonResponse {
        try {
            /** @var User2FaLoginRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: User2FaLoginRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $twoFactor = $emailCodeAuthenticationRepository->findOneBy(['token' => $requestPayload->getCode(), 'uniqueIdentifier' => $requestPayload->getUniqueIdentifier()]);

            if($twoFactor === null) {
                throw new \Exception("The account does not exist");
            }

            if((time() - strtotime($twoFactor->getCreatedAt()->format('Y-m-d H:i:s'))) > 600) {
                throw new \Exception("The link has expired");
            }

            $user = $twoFactor->getUserOwner();
            $jwt = $tokenService->generateJWT($user->getId());

            $entityManager->remove($twoFactor);
            $entityManager->flush();

            return $this->json(
                data: [
                    'status' => 'success',
                    'payload' => [
                        'token' => $jwt
                    ]
                ], status: Response::HTTP_OK);

        } catch (\Exception|Exception $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    #[Route(path: '/logout', name: 'logout', methods: ['GET'])]
    public function logout(): never
    {
        throw new \Exception();
    }
}
