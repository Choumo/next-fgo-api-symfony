# Next FGO Manager (API)

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

## About the project

### Built With
This project was created using the following technologies : 

* Symfony
* Docker

## Getting Started
### Prerequisites
Have Docker installed : https://docs.docker.com/engine/install/
Have Symfony installed : https://symfony.com/doc/current/setup.html

### Installation
To install the project, execute the following commands : 

First, clone the project
```bash
$ git clone https://gitlab.com/Choumo/next-fgo-api-symfony
```

Then go in the created folder
```bash
$ cd next-fgo-api-symfony
```

And run docker
```bash
$ docker compose build --no-cache
$ docker compose up
```

You will be able to access the api on "https://localhost/"

## Usage
