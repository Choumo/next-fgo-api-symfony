<?php

namespace App\Entity;

use App\Repository\CraftEssenceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CraftEssenceRepository::class)]
class CraftEssence
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $ce_id = null;

    #[ORM\Column]
    private ?int $ce_level = null;

    #[ORM\Column]
    private ?bool $isMlb = null;

    #[ORM\ManyToOne(inversedBy: 'craftEssences')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCeId(): ?int
    {
        return $this->ce_id;
    }

    public function setCeId(int $ce_id): static
    {
        $this->ce_id = htmlspecialchars($ce_id);

        return $this;
    }

    public function getCeLevel(): ?int
    {
        return $this->ce_level;
    }

    public function setCeLevel(int $ce_level): static
    {
        $this->ce_level = htmlspecialchars($ce_level);

        return $this;
    }

    public function isIsMlb(): ?bool
    {
        return $this->isMlb;
    }

    public function setIsMlb(bool $isMlb): static
    {
        $this->isMlb = htmlspecialchars($isMlb);

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
