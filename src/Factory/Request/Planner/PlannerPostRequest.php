<?php

namespace App\Factory\Request\Planner;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class PlannerPostRequest extends AbstractRequest
{
    #[SerializedName('currentLoginStreak')]
    #[Assert\NotBlank]
    private int $currentLoginStreak;

    #[SerializedName('currentSaintQuartz')]
    #[Assert\NotBlank]
    private int $currentSaintQuartz;

    #[SerializedName('currentSaintQuartzFragments')]
    #[Assert\NotBlank]
    private int $currentSaintQuartzFragments;

    #[SerializedName('currentSummonTicket')]
    #[Assert\NotBlank]
    private int $currentSummonTicket;

    #[SerializedName('totalLogin')]
    #[Assert\NotBlank]
    private int $totalLogin;

    #[SerializedName('startDate')]
    #[Assert\NotBlank]
    private string $startDate;

    #[SerializedName('endDate')]
    #[Assert\NotBlank]
    private string $endDate;

    public function getCurrentLoginStreak(): int
    {
        return $this->currentLoginStreak;
    }

    public function setCurrentLoginStreak(int $currentLoginStreak): void
    {
        $this->currentLoginStreak = $currentLoginStreak;
    }

    public function getCurrentSaintQuartz(): int
    {
        return $this->currentSaintQuartz;
    }

    public function setCurrentSaintQuartz(int $currentSaintQuartz): void
    {
        $this->currentSaintQuartz = $currentSaintQuartz;
    }

    public function getCurrentSaintQuartzFragments(): int
    {
        return $this->currentSaintQuartzFragments;
    }

    public function setCurrentSaintQuartzFragments(int $currentSaintQuartzFragments): void
    {
        $this->currentSaintQuartzFragments = $currentSaintQuartzFragments;
    }

    public function getTotalLogin(): int
    {
        return $this->totalLogin;
    }

    public function setTotalLogin(int $totalLogin): void
    {
        $this->totalLogin = $totalLogin;
    }

    public function getStartDate(): string
    {
        return $this->startDate;
    }

    public function setStartDate(string $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): string
    {
        return $this->endDate;
    }

    public function setEndDate(string $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getCurrentSummonTicket(): int
    {
        return $this->currentSummonTicket;
    }

    public function setCurrentSummonTicket(int $currentSummonTicket): void
    {
        $this->currentSummonTicket = $currentSummonTicket;
    }




}
