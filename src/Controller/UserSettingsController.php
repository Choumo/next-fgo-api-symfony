<?php

namespace App\Controller;

use App\Entity\CraftEssence;
use App\Entity\Material;
use App\Entity\Quest;
use App\Entity\Servant;
use App\Entity\User;
use App\Exception\ApiException;
use App\Exception\ApiValidationException;
use App\Factory\Request\UserRequest\User2FaRequest;
use App\Factory\Request\UserRequest\UserChangePasswordRequest;
use App\Factory\Request\UserRequest\UserShareDataRequest;
use App\Factory\Request\UserRequest\UserUploadData;
use App\Repository\UserRepository;
use App\Service\Api;
use App\Service\JsonTransformer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/account-settings')]
class UserSettingsController extends AbstractController
{
    /**
     * @param JsonTransformer $jsonTransformer
     * @param ValidatorInterface $validator
     */
    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}


    /**
     * Cette fonction permet à un utilisateur de supprimer son compte
     *
     * @param User $user
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/delete-account', name: 'app_usersettings_deleteaccount', methods: ['DELETE'])]
    #[IsGranted('ROLE_USER')]
    public function deleteAccount(#[CurrentUser] User $user, EntityManagerInterface $entityManager) : JsonResponse
    {
        try {
            $entityManager->remove($user);
            $entityManager->flush();

            return new JsonResponse(
                data: "Your account has been successfully deleted.",
                status: Response::HTTP_NO_CONTENT
            );
        } catch (\Exception|\Throwable $exception) {
            return new JsonResponse(
                data: "Something went wrong",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/share-data', name: 'app_user_sharedata', methods: ["GET", "POST"])]
    #[IsGranted('ROLE_USER')]
    public function shareData(#[CurrentUser] User $user, Request $request, EntityManagerInterface $entityManager) : JsonResponse
    {
        try {
            if($request->isMethod(method: 'GET')) {
                return new JsonResponse(data: [
                    'status' => Response::HTTP_OK,
                    'payload' => [
                        'value' => $user->isShareData()
                    ]
                ], status: Response::HTTP_OK);
            }

            if($request->isMethod(method: 'POST')) {
                /** @var UserShareDataRequest|null $requestPayload */
                $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: UserShareDataRequest::class);

                if (null === $requestPayload) {
                    return new JsonResponse(
                        data: [
                            "Request invalid, please be sure to fill all required inputs."
                        ],
                        status: Response::HTTP_UNPROCESSABLE_ENTITY,
                    );
                }

                try {
                    $payload = $requestPayload->validate($this->validator);
                } catch (ApiValidationException $exception) {
                    return new JsonResponse(
                        data: $exception->getDisplayableViolations(),
                        status: Response::HTTP_UNPROCESSABLE_ENTITY
                    );
                }

                $user->setShareData(shareData: $requestPayload->isShareData());

                $entityManager->persist($user);
                $entityManager->flush();

                return new JsonResponse(data: [
                    'status' => Response::HTTP_OK,
                    'payload' => [
                        'message' => 'Your preferences has been updated.'
                    ]
                ], status: Response::HTTP_OK);

            }
        } catch (\Exception|\Throwable $exception) {
            return new JsonResponse(
                data: "Something went wrong",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * This controller let the user activate the email multifactor authentication
     *
     * @param User $user
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/enable-2fa', name: 'app_settings_enable_2fa', methods: [ 'GET', 'POST' ])]
    #[IsGranted('ROLE_USER')]
    public function enableTwoFA(#[CurrentUser] User $user, EntityManagerInterface $entityManager, Request $request) : JsonResponse
    {

        try {
            if($request->isMethod(method: 'GET')) {
                return new JsonResponse(data: [
                    'status' => Response::HTTP_OK,
                    'payload' => [
                        'value' => $user->isIs2Fa()
                    ]
                ], status: Response::HTTP_OK);
            }

            if($request->isMethod(method: 'POST')) {
                /** @var User2FaRequest|null $requestPayload */
                $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: User2FaRequest::class);

                if (null === $requestPayload) {
                    return new JsonResponse(
                        data: [
                            "Request invalid, please be sure to fill all required inputs."
                        ],
                        status: Response::HTTP_UNPROCESSABLE_ENTITY,
                    );
                }

                try {
                    $payload = $requestPayload->validate($this->validator);
                } catch (ApiValidationException $exception) {
                    return new JsonResponse(
                        data: $exception->getDisplayableViolations(),
                        status: Response::HTTP_UNPROCESSABLE_ENTITY
                    );
                }

                $user->setIs2Fa($requestPayload->isMultiFa());

                $entityManager->persist($user);
                $entityManager->flush();

                return new JsonResponse(data: [
                    'status' => Response::HTTP_OK,
                    'payload' => [
                        'message' => 'Your preferences has been updated.'
                    ]
                ], status: Response::HTTP_OK);

            }
        } catch (\Exception|\Throwable $exception) {
            return new JsonResponse(
                data: "Something went wrong",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * This controller let the current logged user to change his password
     *
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordHasherInterface $passwordHasher
     * @return JsonResponse
     */
    #[Route('/update-password', name: 'app_usersettings_updatepassword', methods: ['PUT'])]
    #[IsGranted('ROLE_USER')]
    public function changePassword(#[CurrentUser] User $user, Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher): JsonResponse
    {
        try {
            /** @var UserChangePasswordRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: UserChangePasswordRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }



            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }


            $hashedPassword = $passwordHasher->hashPassword(user: $user, plainPassword: $requestPayload->getPassword());
            $user->setPassword($hashedPassword);

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json(
                data: [
                    'status' => 'success',
                    'message' => 'Your password has been update.',
                ], status: Response::HTTP_OK);

        } catch (\Exception|\Throwable $exception) {
            return new JsonResponse(
                data: "Something went wrong",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * This controller let the user download all the data of his account
     *
     * @param User $user
     * @return JsonResponse|Response
     */
    #[Route('/download-user-data', name: 'app_download_user_data', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function downloadUserData(#[CurrentUser] User $user) : JsonResponse|Response
    {
        try {
            $userData = [];

            $userServants = $user->getServants();
            $userCraftEssences = $user->getCraftEssences();
            $userMaterials = $user->getMaterials();
            $userQuests = $user->getQuests();

            $userData['user'] = [
                "email" => $user->getEmail(),
                "shareData" => $user->isShareData()
            ];

            foreach ($userServants as $currentUserServant) {
                $userData['servants'][] = [
                    'ServantId' => $currentUserServant->getServantId(),
                    'ServantLevel' => $currentUserServant->getServantLevel()  ,
                    'ServantAppend1' => $currentUserServant->getServantLevelAppend1(),
                    'ServantAppend2' => $currentUserServant->getServantLevelAppend2(),
                    'ServantAppend3' => $currentUserServant->getServantLevelAppend3(),
                    'ServantSkill1' => $currentUserServant->getServantLevelAppend1(),
                    'ServantSkill2' => $currentUserServant->getServantLevelAppend2(),
                    'ServantSkill3' => $currentUserServant->getServantLevelAppend3(),

                ];
            }

            foreach ($userCraftEssences as $currentUserCraftEssence) {
                $userData['craftEssences'][] = [
                    'CraftEssenceId' => $currentUserCraftEssence->getCeId(),
                    'CraftEssenceLevel' => $currentUserCraftEssence->getCeLevel()
                ];
            }

            foreach ($userMaterials as $currentUserMaterials) {
                $userData['materials'] = [
                    'materialId' => $currentUserMaterials->getMaterialId(),
                    'materialQuantity' => $currentUserMaterials->getMaterialQuantity()
                ];
            }

            foreach ($userQuests as $currentUserQuests) {
                $userData['quests'][] = [
                    'questId' => $currentUserQuests->getChapterId(),
                    'questState' => $currentUserQuests->getChapterState()
                ];
            }

            return new JsonResponse(
                data: $userData,
                status: Response::HTTP_OK,
            );

        } catch (\Exception|\Throwable $exception) {
            return new JsonResponse(
                data: "Something went wrong",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet à un utilisateur d'uploader ses données
     *
     * @param User $user
     * @param Api $rayshiftApi
     * @param Request $request
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/upload-user-data', name: 'app_upload_user_data', methods: ["POST"])]
    #[IsGranted('ROLE_USER')]
    public function uploadUserData(#[CurrentUser] User $user, Api $rayshiftApi, Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager): JsonResponse {
        try {
            /** @var UserUploadData|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: UserUploadData::class);
            dd($requestPayload);
            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Request invalid, please be sure to fill all required inputs."
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }


            $requestServants = $requestPayload->getServants();
            if($requestServants !== null) {
                $userServants = $user->getServants();
                foreach ($userServants as $userServant) {
                    $entityManager->remove($userServant);
                }

                foreach($requestServants as $currentServant) {
                    $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/servant/'. $currentServant['ServantId'] .'?lang=en', cacheKey: 'servant-'.$currentServant['ServantId']);

                    $servant = new Servant();
                    $servant->setServantId($currentServant['ServantId']);
                    $servant->setServantLevel($currentServant['ServantLevel']);
                    $servant->setServantLevelSkill1($currentServant['ServantSkill1']);
                    $servant->setServantLevelSkill2($currentServant['ServantSkill2']);
                    $servant->setServantLevelSkill3($currentServant['ServantSkill3']);
                    $servant->setServantLevelAppend1($currentServant['ServantAppend1']);
                    $servant->setServantLevelAppend2($currentServant['ServantAppend2']);
                    $servant->setServantLevelAppend3($currentServant['ServantAppend3']);
                    $servant->setUser($user);

                    $entityManager->persist($servant);
                }
            }

            $requestCraftEssences = $requestPayload->getCraftEssences();
            if($requestCraftEssences !== null) {
                $userCraftEssences = $user->getCraftEssences();
                foreach ($userCraftEssences as $userCraftEssence) {
                    $entityManager->remove($userCraftEssence);
                }

                foreach ($requestCraftEssences as $currentCraftEssence) {

                    $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/equip/' . $currentCraftEssence['CraftEssenceId'] . '?lang=en', cacheKey: 'craft-essence-' . $currentCraftEssence['CraftEssenceId']);
                    $craftEssence = new CraftEssence();
                    $craftEssence->setCeId($currentCraftEssence['CraftEssenceId']);
                    $craftEssence->setCeLevel($currentCraftEssence['CraftEssenceLevel']);
                    $craftEssence->setUser($user);

                    $entityManager->persist($craftEssence);

                }
            }

            $requestMaterials = $requestPayload->getMaterials();
            if($requestMaterials !== null) {
                $userMaterials = $user->getMaterials();
                foreach($userMaterials as $userMaterial) {
                    $entityManager->remove($userMaterial);
                }

                foreach ($requestMaterials as $currentMaterial) {

                    $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/item/' . $currentMaterial['materialId'] . '?lang=en', cacheKey: 'item-' . $currentMaterial['materialId']);

                    $material = new Material();
                    $material->setMaterialId($currentMaterial['materialId']);
                    $material->setMaterialQuantity($currentMaterial['materialQuantity']);
                    $material->setMaterialUser($user);

                    $entityManager->persist($material);
                }
            }

            $requestQuests = $requestPayload->getQuests();
            if($requestQuests !== null) {
                $userQuests = $user->getQuests();
                foreach ($userQuests as $userQuest) {
                    $entityManager->remove($userQuest);
                }

                foreach($requestQuests as $currentQuest) {

                    $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/war/'. $currentQuest['questId'] .'?lang=en', cacheKey: "story-" . $currentQuest['questId']);

                    $quest = new Quest();
                    $quest->setChapterId($currentQuest['questId']);
                    $quest->setChapterState($currentQuest['questState']);
                    $quest->setUser($user);

                    $entityManager->persist($quest);
                }
            }

            $entityManager->flush();

            return new JsonResponse(
                data: "You data have been updated.",
                status: Response::HTTP_OK
            );
        } catch (\Exception|\Throwable|ApiException $exception) {
            return new JsonResponse(
                data: "Something went wrong",
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

}
