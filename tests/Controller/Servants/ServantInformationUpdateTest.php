<?php

namespace App\Tests\Controller\Servants;

use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServantInformationUpdateTest extends WebTestCase
{
    /**
     * Cette fonction permet de tester la modification des informations d'un servant si l'utilisateur est connecté
     *
     * Doit retourner un code 200
     * @return void
     * @throws Exception
     */
    public function testUpdateServantInformationIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response = $client->request("POST", "/servants/user/100/update", [
            "id" => 100,
            "name" => "somestring",
            "class" => "somestring",
            "rarity" => 5,
            "graph" => "somestring",
            "level" => 100,
            "bond_level" => 10,
            "np_level" => 3,
            "servant_level_skill_1" => 10,
            "servant_level_skill_2" => 10,
            "servant_level_skill_3" => 10,
            "servant_level_append_1" => 10,
            "servant_level_append_2" => 10,
            "servant_level_append_3" => 10,
        ]);

        $this->assertResponseIsSuccessful();
    }

    /**
     * Cette fonction permet de tester la modification des informations d'un servant si l'utilisateur est connecté
     * Mais que le servant à modifier n'existe pas dans l'API
     *
     * Doit retourner un code 422
     * @return void
     * @throws Exception
     */
    public function testUpdateServantInformationIfUserLoggedAndServantIsNotInUserDatabase(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response = $client->request("POST", "/servants/user/100000000/update", [
            "id" => 100,
            "name" => "somestring",
            "class" => "somestring",
            "rarity" => 5,
            "graph" => "somestring",
            "level" => 100,
            "bond_level" => 10,
            "np_level" => 3,
            "servant_level_skill_1" => 10,
            "servant_level_skill_2" => 10,
            "servant_level_skill_3" => 10,
            "servant_level_append_1" => 10,
            "servant_level_append_2" => 10,
            "servant_level_append_3" => 10,
        ]);

        $this->assertResponseStatusCodeSame(422);
    }

    /**
     * Cette fonction permet de tester la modification des informations d'un servant si l'utilisateur est connecté
     * Mais que la requete n'est pas conforme à ce qui est attendu par notre API
     *
     * Doit retourner un code 422
     * @return void
     * @throws Exception
     */
    public function testUpdateServantInformationIfUserLoggedAndRequestBodyNotGood(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response = $client->request("POST", "/servants/user/100/update", [
            "servant_level" => 100,
            "servant_level_skill_1" => 10,
            "servant_level_skill_2" => 10,
            "servant_level_skill_29" => 1000,
            "sddddddd" => 10,
            "servant_level_append_2" => 10,
            "servant_level_append_4" => 10,
        ]);

        $this->assertResponseStatusCodeSame(422);
    }

    /**
     * Cette fonction permet de tester la modification des informations d'un servant si l'utilisateur n'est pas connecté
     *
     * Doit retourner un code 401
     * @return void
     * @throws Exception
     */
    public function testUpdateServantInformationIfUserNotLogged(): void
    {
        $client = static::createClient();

        $response = $client->request("POST", "/servants/user/100/update", [
            "id" => 100,
            "name" => "somestring",
            "class" => "somestring",
            "rarity" => 5,
            "graph" => "somestring",
            "level" => 100,
            "bond_level" => 10,
            "np_level" => 3,
            "servant_level_skill_1" => 10,
            "servant_level_skill_2" => 10,
            "servant_level_skill_3" => 10,
            "servant_level_append_1" => 10,
            "servant_level_append_2" => 10,
            "servant_level_append_3" => 10,
        ]);

        $this->assertResponseStatusCodeSame(401);
    }
}
