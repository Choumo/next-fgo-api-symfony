<?php

namespace App\Controller;

use App\Entity\CraftEssence;
use App\Entity\Servant;
use App\Entity\User;
use App\Exception\ApiException;
use App\Repository\CraftEssenceRepository;
use App\Repository\UserRepository;
use App\Service\Api;
use App\Service\Statistics\Global\CraftEssenceStatistics;
use App\Service\Statistics\Global\ServantStatistics;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/craft-essence')]
class CraftEssenceController extends AbstractController
{
    /**
     * Cette fonction sert a récupérer la liste des Craft Essences existant en jeu
     *
     * @param Api $rayshiftApi
     * @param EntityManagerInterface $em
     * @param User $user
     * @param CraftEssenceRepository $craftEssenceRepository
     * @return JsonResponse
     */
    #[Route('/list', name: 'app_craft_essence', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function index(Api $rayshiftApi, EntityManagerInterface $em, #[CurrentUser] User $user, CraftEssenceRepository $craftEssenceRepository): JsonResponse
    {
        try {
            $craft_essence_array = [];
            $response = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_equip_lang_en.json', cacheKey: 'craft-essence-list');
            $count_craft_essence = count($response);

            for ($i = 0; $i < $count_craft_essence; $i++) {
                $user_has_craft_essence = $craftEssenceRepository->findOneBy(["ce_id" => $response[$i]['collectionNo'], "user" => $user]);
                $craft_essence_array[] = [
                    'id' => $response[$i]['collectionNo'],
                    'name' => $response[$i]['name'],
                    'type' => $response[$i]['flag'],
                    'rarity' => $response[$i]['rarity'],
                    'graph' => $response[$i]['extraAssets']['charaGraph']['equip'][$response[$i]['id']],
                    'isObtained' => (bool)$user_has_craft_essence
                ];
            }

            return new JsonResponse(
                data: $craft_essence_array,
                status: Response::HTTP_OK);
        } catch(ApiException $e) {
            return new JsonResponse(
                data: [
                    "status" => "error",
                    "message" => $e->getMessage()
                ], status: Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Cette fonction permet d'ajouter une Craft Essence à la collection de l'utilisateur connecté
     *
     * @param int $id
     * @param EntityManagerInterface $em
     * @param Api $rayshiftApi
     * @param CraftEssenceRepository $craftEssenceRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/add/{id}', name: 'app_add_craft_essence', methods: ["POST"])]
    #[IsGranted('ROLE_USER')]
    public function addCraftEssenceToUser(#[\SensitiveParameter] int $id, EntityManagerInterface $em, Api $rayshiftApi, CraftEssenceRepository $craftEssenceRepository, #[CurrentUser] User $user): JsonResponse
    {
        try {
            $craftEssence = $craftEssenceRepository->findOneBy(['ce_id' => $id, 'user' => $user]);
            $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/equip/'. $id .'?lang=en', cacheKey: 'craft-essence-' . $id);

            if ($craftEssence) {
                $em->remove($craftEssence);
                $em->flush();

                return new JsonResponse(
                    data: [
                        "status" => "success",
                        "message" => "The craft essence has been deleted"
                    ], status: Response::HTTP_CREATED);
            } else {
                $craftEssence = new CraftEssence();
                $craftEssence->setCeId($id);
                $craftEssence->setIsMlb(false);
                $craftEssence->setCeLevel(1);
                $craftEssence->setUser($this->getUser());

                $em->persist($craftEssence);
                $em->flush();

                return new JsonResponse(["status" => "success", "message" => "The craft essence has been added to your database"], Response::HTTP_CREATED);
            }
        } catch (\Exception $e) {
            return new JsonResponse(["status" => "error", "message" => $e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Cette fonction permet d'afficher la liste des Craft Essences que l'utilisateur as dans son inventaire
     *
     * @param Api $rayshift_api
     * @param CraftEssenceRepository $craftEssenceRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/user/list', name: 'app_craft_essence_list_all_craft_essences_of_user', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public  function listAllCraftEssencesOfUser(Api $rayshift_api, CraftEssenceRepository $craftEssenceRepository, #[CurrentUser] User $user): JsonResponse
    {
        try{
            $user_craft_essences = [];

            $craftEssenceList = $craftEssenceRepository->findBy(['user' => $user]);

            foreach($craftEssenceList as $currentCraftEssence){
                $craft_essence_info = $rayshift_api->callRayshiftApi(endpoint: 'nice/JP/equip/' . $currentCraftEssence->getCeId() . '?lang=en', cacheKey: 'craft-essence-' . $currentCraftEssence->getCeId());
                $user_craft_essences[] = [
                    'name' => $craft_essence_info['name'],
                    'type' => $craft_essence_info['flag'],
                    'graph' => $craft_essence_info['extraAssets']['charaGraph']['equip'][$craft_essence_info['id']],
                    'rarity' => $craft_essence_info['rarity'],
                    'craft_essence_id' => $currentCraftEssence->getCeId(),
                    'craft_essence_level' => $currentCraftEssence->getCeLevel(),
                    'isMlb' => $currentCraftEssence->isIsMlb()
                ];
            }

            return new JsonResponse(
                data: $user_craft_essences,
                status: Response::HTTP_OK
            );
        }catch(ApiException $e){
            return new JsonResponse(
                data: $e->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Cette fonction permet de récupérer les statistiques de l'utilisateur connecté pour les craft essence
     *
     * @param User $user
     * @param CraftEssenceRepository $craftEssenceRepository
     * @param Api $rayshiftApi
     * @return JsonResponse
     */
    #[Route('/statistics', name: 'app_craft_essence_user_statistics', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function userStatistics(#[CurrentUser] User $user, CraftEssenceRepository $craftEssenceRepository, Api $rayshiftApi): JsonResponse
    {
        try {
            $craftEssencesOfUser = $craftEssenceRepository->findBy(["user" => $user]);
            $craftEssencesOfApi = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_equip_lang_en.json', cacheKey: 'craft-essence-list');

            $fiveStarsApiCE = 0;
            $fourStarsApiCE = 0;
            $threeStarsApiCE = 0;
            $twoStarsApiCE = 0;
            $oneStarsApiCE = 0;

            $fiveStarsUserCE = 0;
            $fourStarsUserCE = 0;
            $threeStarsUserCE = 0;
            $twoStarsUserCE = 0;
            $oneStarsUserCE = 0;

            $ceByType = [
                "svtEquipFriendShip" => 0,
                "svtEquipCampaign" => 0,
                "svtEquipEvent" => 0,
                "svtEquipEventReward" => 0,
                "svtEquipManaExchange" => 0,
                "normal" => 0,
                "unknown" => 0
            ];

            foreach ($craftEssencesOfApi as $craftEssence) {
                switch ($craftEssence["rarity"]) {
                    case 5:
                        $fiveStarsApiCE = $fiveStarsApiCE + 1;
                        break;
                    case 4:
                        $fourStarsApiCE = $fourStarsApiCE + 1;
                        break;
                    case 3:
                        $threeStarsApiCE = $threeStarsApiCE + 1;
                        break;
                    case 2:
                        $twoStarsApiCE = $twoStarsApiCE + 1;
                        break;
                    case 1:
                        $oneStarsApiCE = $oneStarsApiCE + 1;
                        break;
                }

                foreach ($craftEssencesOfUser as $userCraftEssence) {
                    if($userCraftEssence->getCeId() === $craftEssence["collectionNo"]) {
                        switch ($craftEssence["rarity"]) {
                            case 5:
                                $fiveStarsUserCE = $fiveStarsUserCE + 1;
                                break;
                            case 4:
                                $fourStarsUserCE = $fourStarsUserCE + 1;
                                break;
                            case 3:
                                $threeStarsUserCE = $threeStarsUserCE + 1;
                                break;
                            case 2:
                                $twoStarsUserCE = $twoStarsUserCE + 1;
                                break;
                            case 1:
                                $oneStarsUserCE = $oneStarsUserCE + 1;
                                break;
                        }

                        $ceByType[$craftEssence["flag"]] = $ceByType[$craftEssence["flag"]] + 1;
                    }
                }
            }

            $userStatistics = [
                "totalCraftEssenceObtained" => count($craftEssencesOfUser),
                "totalCraftEssenceUnobtained" => (count($craftEssencesOfApi) - count($craftEssencesOfUser)),
                "totalCraftEssenceObtainedPercentage" => round((count($craftEssencesOfUser) / count($craftEssencesOfApi) * 100)),
                "craftEssenceRarityProgress" => [
                    "fiveStars" => [
                        "user" => $fiveStarsUserCE,
                        "game" => $fiveStarsApiCE,
                    ],
                    "fourStars" => [
                        "user" => $fourStarsUserCE,
                        "game" => $fourStarsApiCE,
                    ],
                    "threeStars" => [
                        "user" => $threeStarsUserCE,
                        "game" => $threeStarsApiCE,
                    ],
                    "twoStars" => [
                        "user" => $twoStarsUserCE,
                        "game" => $twoStarsApiCE,
                    ],
                    "oneStars" => [
                        "user" => $oneStarsUserCE,
                        "game" => $oneStarsApiCE,
                    ],
                ],
                "craftEssenceTypeStatistics" => $ceByType
            ];

            return new JsonResponse(
                data: $userStatistics,
                status: Response::HTTP_OK
            );

        } catch (ApiException $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Cette fonction permet de récupérer les statistiques globales pour les craft essence
     *
     * @param CraftEssenceRepository $servantRepository
     * @param Api $rayshiftApi
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    #[Route('/global-statistics', name: 'app_craftessence_global_statistics', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function globalStatistics(CraftEssenceRepository $servantRepository, Api $rayshiftApi, UserRepository $userRepository): JsonResponse
    {
        try {
            $users = $userRepository->findAll();
            $craftEssenceOfApi = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_equip_lang_en.json', cacheKey: 'craft-essence-list');
            $craftEssenceStatistics = new CraftEssenceStatistics();

            foreach ($users as $user) {
                $craftEssenceOfUser = $user->getCraftEssences();

                if($user->isShareData() === true) {
                    foreach ($craftEssenceOfApi as $apiCraftEssence) {
                       foreach ($craftEssenceOfUser as $userCraftEssence) {
                            if ($apiCraftEssence['collectionNo'] === $userCraftEssence->getCeId()) {
                                switch ($apiCraftEssence["rarity"]) {
                                    case 5:
                                        $craftEssenceStatistics->setFiveStar(fiveStar: 1);
                                        break;
                                    case 4:
                                        $craftEssenceStatistics->setFourStar(fourStar: 1);
                                        break;
                                    case 3:
                                        $craftEssenceStatistics->setThreeStar(threeStar: 1);
                                        break;
                                    case 2:
                                        $craftEssenceStatistics->setTwoStar(twoStar: 1);
                                        break;
                                    case 1:
                                        $craftEssenceStatistics->setOneStar(oneStar: 1);
                                        break;
                                }

                                $craftEssenceStatistics->setCraftEssenceType($apiCraftEssence["flag"], 1);
                            }
                        }
                    }
                }
            }

            return new JsonResponse(
                data: $craftEssenceStatistics->getStatistics(),
                status: Response::HTTP_OK
            );

        } catch (\Exception|ApiException $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

}
