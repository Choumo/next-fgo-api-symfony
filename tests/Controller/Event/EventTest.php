<?php

namespace App\Tests\Controller\Event;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;
class EventTest extends WebTestCase
{
    public function testShowListOfEventIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response = $client->request('GET', '/event/list');

        $this->assertResponseIsSuccessful();
    }

    public function testShowListOfEventIfUserNotLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);

        $response = $client->request('GET', '/event/list');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testShowEventInformationIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response = $client->request('GET', '/event/104');

        $this->assertResponseIsSuccessful();
    }

    public function testShowEventInformationIfUserNotLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);

        $response = $client->request('GET', '/event/104');

        $this->assertResponseStatusCodeSame(401);
    }

    public function testShowEventInformationIfEventDontExistsInApi(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepository = $container->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "email@provider.fr"]);

        $response = $client->request('GET', '/event/104');

        $this->assertResponseStatusCodeSame(401);
    }
}
