<?php

namespace App\Factory\Request\ServantRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class ServantPatchRequest extends AbstractRequest
{
    #[SerializedName('id')]
    #[Assert\NotBlank]
    private int $id;
    #[SerializedName('name')]
    #[Assert\NotBlank]
    private string $name;
    #[SerializedName('class')]
    #[Assert\NotBlank]
    private string $class;
    #[SerializedName('rarity')]
    #[Assert\NotBlank]
    private int $rarity;
    #[SerializedName('graph')]
    #[Assert\NotBlank]
    private string $graph;
    #[SerializedName('level')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 120)]
    private int $level;

    #[SerializedName('bond_level')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 15)]
    private int $bond_level;

    #[SerializedName('np_level')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 5)]
    private int $np_level;
    #[SerializedName('servant_skill_1')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private int $servantSkill1;
    #[SerializedName('servant_skill_2')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private int $servantSkill2;
    #[SerializedName('servant_skill_3')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private int $servantSkill3;
    #[SerializedName('servant_append_1')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private int $servantAppend1;
    #[SerializedName('servant_append_2')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private int $servantAppend2;
    #[SerializedName('servant_append_3')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private int $servantAppend3;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class): void
    {
        $this->class = $class;
    }

    /**
     * @return int
     */
    public function getRarity(): int
    {
        return $this->rarity;
    }

    /**
     * @param int $rarity
     */
    public function setRarity(int $rarity): void
    {
        $this->rarity = $rarity;
    }

    /**
     * @return string
     */
    public function getGraph(): string
    {
        return $this->graph;
    }

    /**
     * @param string $graph
     */
    public function setGraph(string $graph): void
    {
        $this->graph = $graph;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getServantSkill1(): int
    {
        return $this->servantSkill1;
    }

    /**
     * @param int $servantSkill1
     */
    public function setServantSkill1(int $servantSkill1): void
    {
        $this->servantSkill1 = $servantSkill1;
    }

    /**
     * @return int
     */
    public function getServantSkill2(): int
    {
        return $this->servantSkill2;
    }

    /**
     * @param int $servantSkill2
     */
    public function setServantSkill2(int $servantSkill2): void
    {
        $this->servantSkill2 = $servantSkill2;
    }

    /**
     * @return int
     */
    public function getServantSkill3(): int
    {
        return $this->servantSkill3;
    }

    /**
     * @param int $servantSkill3
     */
    public function setServantSkill3(int $servantSkill3): void
    {
        $this->servantSkill3 = $servantSkill3;
    }

    /**
     * @return int
     */
    public function getServantAppend1(): int
    {
        return $this->servantAppend1;
    }

    /**
     * @param int $servantAppend1
     */
    public function setServantAppend1(int $servantAppend1): void
    {
        $this->servantAppend1 = $servantAppend1;
    }

    /**
     * @return int
     */
    public function getServantAppend2(): int
    {
        return $this->servantAppend2;
    }

    /**
     * @param int $servantAppend2
     */
    public function setServantAppend2(int $servantAppend2): void
    {
        $this->servantAppend2 = $servantAppend2;
    }

    /**
     * @return int
     */
    public function getServantAppend3(): int
    {
        return $this->servantAppend3;
    }

    /**
     * @param int $servantAppend3
     */
    public function setServantAppend3(int $servantAppend3): void
    {
        $this->servantAppend3 = $servantAppend3;
    }

    public function getBondLevel(): int
    {
        return $this->bond_level;
    }

    public function setBondLevel(int $bond_level): void
    {
        $this->bond_level = $bond_level;
    }

    public function getNpLevel(): int
    {
        return $this->np_level;
    }

    public function setNpLevel(int $np_level): void
    {
        $this->np_level = $np_level;
    }


}
