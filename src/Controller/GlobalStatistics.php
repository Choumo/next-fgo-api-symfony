<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\ApiValidationException;
use App\Factory\Request\UserRequest\UserShareDataRequest;
use App\Service\JsonTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/global-statistics')]
class GlobalStatistics extends AbstractController
{

}
