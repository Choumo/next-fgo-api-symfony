<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\ApiException;
use App\Service\Api;
use SensitiveParameter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/events')]
class EventController extends AbstractController
{
    /**
     * Cette fonction permet de récupérer la liste des évenements en jeu
     *
     * @param Api $rayshiftApi
     * @return JsonResponse
     */
    #[Route('/list', name: 'app_event', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function eventList(Api $rayshiftApi): JsonResponse
    {
        try {
            $event_array = [];

            $response = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_war_lang_en.json', cacheKey: 'war-list');
            $count_story = count($response);

            for($i = 0; $i < $count_story; $i++){
                if(!in_array("isEvent", $response[$i]['flags'])){
                    unset($response[$i]);
                }else{
                    $event_array[] = [
                        'id' => $response[$i]['id'],
                        'name' => $response[$i]['longName'],
                        'banner' => $response[$i]['banner'],
                    ];
                }
            }

            return new JsonResponse(data: $event_array, status: Response::HTTP_OK);
        } catch(ApiException $e) {
            return new JsonResponse(
                data: [
                    "status" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "message" => $e->getMessage()
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Cette fonction permet de récupérer la liste des matériaux d'un event sélectionné
     *
     * @param int $id
     * @param User $user
     * @param Api $rayshift_api
     * @return JsonResponse
     */
    #[Route('/{id}', name: 'app_event_info', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function eventInfo(#[SensitiveParameter] int $id, #[CurrentUser] User $user, Api $rayshift_api): JsonResponse
    {
        try {
            $response = $rayshift_api->callRayshiftApi(endpoint: 'nice/JP/war/'. $id .'?lang=en', cacheKey: "event-" . $id);
            $quest_array = [];

            $quest_array['id'] = $response['id'];
            $quest_array['name'] = $response['longName'];
            $quest_array['rewards'] = [];

            foreach($response["spots"] as $current_spot){
                foreach($current_spot["quests"] as $current_quest) {
                    foreach($current_quest["gifts"] as $current_gift){
                        if($current_gift["type"] === "item"){
                            $giftInformation = $rayshift_api->callRayshiftApi(endpoint: 'nice/JP/item/' . $current_gift["objectId"] . '?lang=en' ,cacheKey: 'material-' . $current_gift["objectId"]);
                            $quest_array['rewards'][$current_gift["objectId"]] = [
                                "id" => $current_gift["objectId"],
                                "name" => $giftInformation["name"],
                                "quantity" => isset($quest_array['rewards'][$current_gift['objectId']]) ? $quest_array['rewards'][$current_gift["objectId"]]['quantity'] + $current_gift["num"] : $current_gift["num"],
                                "background" => $giftInformation["background"],
                                "icon" => $giftInformation["icon"]
                            ];

                        }
                    }
                }
            }

            return new JsonResponse(data: $quest_array, status: Response::HTTP_OK);
        } catch(ApiException $e) {
            return new JsonResponse(
                data: [
                    "status" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "message" => $e->getMessage()
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
