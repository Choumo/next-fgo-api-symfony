<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Token
{
    private string $jwtSecretKey;
    private string $jwtHashAlg;

    private int $jwtExpTime;

    function __construct(ParameterBagInterface $parameterBag)
    {
        $this->jwtSecretKey = $parameterBag->get('app.jwt_secret');
        $this->jwtHashAlg = $parameterBag->get('app.jwt_hash_alg');
        $this->jwtExpTime = $parameterBag->get('app.jwt_exp_time');
    }

    /**
     * Generate a JWT Token
     *
     * @param int $id
     * @return string
     */
    public function generateJWT(int $id): string
    {
        $headers = ['alg' => $this->jwtHashAlg];
        $payload = [
            'sub' => $id,
            'iat' => time(),
            'exp' => time() + $this->jwtExpTime,
        ];

        $base64_header = $this->base64url_encode(json_encode($headers));
        $base64_payload = $this->base64url_encode(json_encode($payload));

        $signature = hash_hmac($headers['alg'], $base64_header . "." . $base64_payload, $this->jwtSecretKey);
        $base64_signature = $this->base64url_encode($signature);

        $jwt = $base64_header . "." . $base64_payload . "." . $base64_signature;

        return $jwt;
    }

    /**
     * Decode the content of a JWT Token
     *
     * @param string $jwt
     * @return array
     */
    public function decodeJWT(string $jwt): array
    {
        [$headers, $payload, $signature] = explode('.', $jwt);

        return [
            json_decode($this->base64url_decode($headers), true),
            json_decode($this->base64url_decode($payload), true),
            $this->base64url_decode($signature),
        ];
    }

    /**
     * Verify the signature of a JWT Token
     *
     * @param array $token_headers
     * @param array $token_payload
     * @param string $token_signature
     * @return bool
     */
    public function verifySignature(array $token_headers, array $token_payload, string $token_signature): bool
    {
        $base64_header = $this->base64url_encode(json_encode($token_headers));
        $base64_payload = $this->base64url_encode(json_encode($token_payload));

        $token_hash_alg = $token_headers['alg'];

        $signature = hash_hmac($token_hash_alg, $base64_header . "." . $base64_payload, $this->jwtSecretKey);

        if($signature !== $token_signature){
            return false;
        }

        return true;
    }

    /**
     * @param mixed $data
     * @return string
     */
    public function base64url_encode(mixed $data): string
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * @param string $base64_string
     * @return string
     */
    public function base64url_decode(string $base64_string): string
    {
        return base64_decode(str_pad(strtr($base64_string, '-_', '+/'), strlen($base64_string) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * @param int $id
     * @return string
     * @throws \Random\RandomException
     */
    public function generatePasswordResetToken(int $id): string
    {
        $options = [
            'cost' => 12,
        ];

        return str_replace("/", "_", password_hash(uniqid() . $id, PASSWORD_BCRYPT, $options));
    }

}
