<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use App\Factory\Request\UserRequest\UploadData\ServantObject;
use App\Factory\Request\UserRequest\UploadData\UserObject;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\SerializedName;

class UserUploadData extends AbstractRequest {

    #[SerializedName('user')]
    private UserObject $user;

    /**
     * @var ServantObject[] $servants
     */
    #[SerializedName('servants')]
    private $servants = [];

    #[SerializedName('craftEssences')]
    private array $craftEssences = [];

    #[SerializedName('materials')]
    private array $materials = [];

    #[SerializedName('quests')]
    private array $quests = [];

    /**
     * @return UserObject
     */
    public function getUser(): UserObject
    {
        return $this->user;
    }

    /**
     * @param UserObject $user
     */
    public function setUser(UserObject $user): void
    {
        $this->user = $user;
    }

    public function getServants(): array
    {
        return $this->servants;
    }

    public function getCraftEssences() : array {
        return $this->craftEssences;
    }

    public function getMaterials(): array {
        return $this->materials;
    }

    public function getQuests(): array {
        return $this->quests;
    }
}
