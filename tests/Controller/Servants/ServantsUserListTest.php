<?php

namespace App\Tests\Controller\Servants;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServantsUserListTest extends WebTestCase
{
    public function testShowListOfServantsOfUserIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $reponse_servant_list = $client->request("GET", "/servants/user/list");

        $this->assertResponseIsSuccessful();
    }

    public function testShowListOfServantsOfUserIfUserNotLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->request("GET", "/servants/user/list");

        $this->assertResponseStatusCodeSame(401);
    }


}
