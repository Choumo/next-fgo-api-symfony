<?php

namespace App\Tests\Controller\Servants;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServantsTest extends WebTestCase {

    public function testAddServantToUserIfUserLogged() : void {

        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $reponse_servant_list = $client->request("POST", "/servants/add/100", []);

        $this->assertResponseIsSuccessful();
    }

    public function testAddServantToUserIfUserNotLogged() : void {

        $client = static::createClient();

        $reponse_servant_list = $client->request("POST", "/servants/add/100", []);

        $this->assertResponseStatusCodeSame(401);
    }

    public function testAddServantToDatabaseIfServantDoesNotExistsInApi(): void
    {


        $client = static::createClient();

        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);

        $reponse_servant_list = $client->request("POST", "/servants/add/1006446874877", []);

        $this->assertResponseStatusCodeSame(401);
    }

}
