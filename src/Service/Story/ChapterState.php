<?php

namespace App\Service\Story;

class ChapterState
{
    private int $cleared = 0;

    private int $inProgress = 0;

    private int $notDone = 0;

    /**
     * @return int
     */
    public function getCleared(): int
    {
        return $this->cleared;
    }

    /**
     * @param int $cleared
     */
    public function setCleared(int $cleared): void
    {
        $this->cleared = $this->cleared + $cleared;
    }

    /**
     * @return int
     */
    public function getInProgress(): int
    {
        return $this->inProgress;
    }

    /**
     * @param int $inProgress
     */
    public function setInProgress(int $inProgress): void
    {
        $this->inProgress = $this->inProgress + $inProgress;
    }

    /**
     * @return int
     */
    public function getNotDone(): int
    {
        return $this->notDone;
    }

    /**
     * @param int $notDone
     */
    public function setNotDone(int $notDone): void
    {
        $this->notDone = $this->notDone + $notDone;
    }

    public function dataAsArray(): array
    {
        return [
            'cleared' => $this->cleared,
            'inProgress' => $this->inProgress,
            'notDone' => $this->notDone,
        ];
    }

}
