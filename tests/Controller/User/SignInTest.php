<?php

namespace App\Tests\Controller\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SignInTest extends WebTestCase {


    // ???? Marche pas lol
    public function testLoginSuccess(): void
    {

        $response = static::createClient()->request(
            method: 'POST',
            uri: '/api/user/login',
            content: json_encode(
                [
                    'username' => "email3@provider.fr",
                    'password' => "NqR1frt98M8Z88PH9SpZ0GbHj9ZQh",
                ]
            )
        );

        $this->assertResponseIsSuccessful();
    }


    public function testLoginFailureWithNoParameters(): void
    {

        $response = static::createClient()->request('POST', '/user/signin');

        $this->assertResponseStatusCodeSame(401);
    }


    public function testLoginFailureWithNoEmail(): void
    {

        $response = static::createClient()->request('POST', '/user/signin', ['password' => 'Somep4ssWord!']);

        $this->assertResponseStatusCodeSame(401);
    }

    public function testLoginFailureWithNoPassword(): void
    {

        $response = static::createClient()->request('POST', '/user/signin', ['email' => 'email@provider.fr']);

        $this->assertResponseStatusCodeSame(401);
    }


    public function testLoginFailureWithInvalidEmail(): void
    {

        $response = static::createClient()->request('POST', '/user/signin', ['email' => 'email@provider.com', 'password' => 'Somep4ssWord!']);

        $this->assertResponseStatusCodeSame(401);
    }


    public function testLoginFailureWithInvalidPassword(): void
    {

        $response = static::createClient()->request('POST', '/user/signin', ['email' => 'email@provider.fr', 'password' => 'SomeP4ssW0rd!']);

        $this->assertResponseStatusCodeSame(401);
    }
}
