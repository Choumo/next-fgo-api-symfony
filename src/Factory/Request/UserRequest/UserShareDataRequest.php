<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class UserShareDataRequest extends AbstractRequest
{
    #[SerializedName('shareData')]
    #[Assert\Type(
        type: 'bool',
        message: 'The value {{ value }} is not a valid {{ type }}.',
    )]
    private bool $shareData;

    public function isShareData(): bool
    {
        return $this->shareData;
    }

    public function setShareData(bool $shareData): void
    {
        $this->shareData = $shareData;
    }


}
