<?php

namespace App\Factory\Request\ManagerRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class ManagerPostRequest extends AbstractRequest
{
    #[SerializedName('id')]
    #[Assert\NotBlank()]
    private int $id;

    #[SerializedName('name')]
    private string $name;

    #[SerializedName('class')]
    private string $class;

    #[SerializedName('graph')]
    private string $graph;

    #[SerializedName('icon')]
    private string $icon;

    #[SerializedName('rarity')]
    private int $rarity;
    #[SerializedName('skills')]
    private SkillsObject $skills;

    #[SerializedName('appends')]
    private AppendsObject $appends;

    #[SerializedName('target')]
    private TargetsObject $target;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class): void
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getGraph(): string
    {
        return $this->graph;
    }

    /**
     * @param string $graph
     */
    public function setGraph(string $graph): void
    {
        $this->graph = $graph;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return int
     */
    public function getRarity(): int
    {
        return $this->rarity;
    }

    /**
     * @param int $rarity
     */
    public function setRarity(int $rarity): void
    {
        $this->rarity = $rarity;
    }

    /**
     * @return SkillsObject
     */
    public function getSkills(): SkillsObject
    {
        return $this->skills;
    }

    /**
     * @param SkillsObject $skills
     */
    public function setSkills(SkillsObject $skills): void
    {
        $this->skills = $skills;
    }

    /**
     * @return AppendsObject
     */
    public function getAppends(): AppendsObject
    {
        return $this->appends;
    }

    /**
     * @param AppendsObject $appends
     */
    public function setAppends(AppendsObject $appends): void
    {
        $this->appends = $appends;
    }

    /**
     * @return TargetsObject
     */
    public function getTarget(): TargetsObject
    {
        return $this->target;
    }

    /**
     * @param TargetsObject $target
     */
    public function setTarget(TargetsObject $target): void
    {
        $this->target = $target;
    }
}
