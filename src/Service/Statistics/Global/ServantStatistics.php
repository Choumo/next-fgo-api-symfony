<?php

namespace App\Service\Statistics\Global;

class ServantStatistics
{
    /**
     * @var int
     */
    private int $fiveStar = 0;

    /**
     * @var int
     */
    private int $fourStar = 0;

    /**
     * @var int
     */
    private int $threeStar = 0;

    /**
     * @var int
     */
    private int $twoStar = 0;

    /**
     * @var int
     */
    private int $oneStar = 0;

    /**
     * @var array
     */
    private array $servantsClass = [];

    public function __construct()
    {
        $this->servantsClass = [
            "saber" => 0,
            "archer" => 0,
            "lancer" => 0,
            "rider" => 0,
            "caster" => 0,
            "assassin" => 0,
            "berserker" => 0,
            "ruler" => 0,
            "avenger" => 0,
            "moonCancer" => 0,
            "alterEgo" => 0,
            "foreigner" => 0,
            "pretender" => 0,
            "shielder" => 0,
            "beast" => 0,
        ];
    }

    public function getFiveStar(): int
    {
        return $this->fiveStar;
    }

    public function setFiveStar(int $fiveStar): void
    {
        $this->fiveStar = $this->fiveStar + $fiveStar;
    }

    public function getFourStar(): int
    {
        return $this->fourStar;
    }

    public function setFourStar(int $fourStar): void
    {
        $this->fourStar = $this->fourStar + $fourStar;
    }

    public function getThreeStar(): int
    {
        return $this->threeStar;
    }

    public function setThreeStar(int $threeStar): void
    {
        $this->threeStar = $this->threeStar + $threeStar;
    }

    public function getTwoStar(): int
    {
        return $this->twoStar;
    }

    public function setTwoStar(int $twoStar): void
    {
        $this->twoStar = $this->twoStar + $twoStar;
    }

    public function getOneStar(): int
    {
        return $this->oneStar;
    }

    public function setOneStar(int $oneStar): void
    {
        $this->oneStar = $this->oneStar + $oneStar;
    }

    public function getTotalOfServant(): int
    {
        return $this->fiveStar + $this->fourStar + $this->threeStar + $this->twoStar + $this->oneStar;
    }

    public function setServantClass(string $class, int $value): void
    {
        $this->servantsClass[$class] = $this->servantsClass[$class] + $value;
    }

    public function getServantsClass(): array
    {
        return $this->servantsClass;
    }

    public function getStatistics(): array
    {
        return [
            "totalGlobalServantObtained" => $this->getTotalOfServant(),
            "totalGlobalFiveStarServantObtained" => $this->getFiveStar(),
            "totalGlobalFourStarServantObtained" => $this->getFourStar(),
            "totalGlobalThreeStarServantObtained" => $this->getThreeStar(),
            "totalGlobalTwoStarServantObtained" => $this->getTwoStar(),
            "totalGlobalOneStarServantObtained" => $this->getOneStar(),
            "totalGlobalServantObtainedByClass" => $this->getServantsClass(),
        ];
    }
}
