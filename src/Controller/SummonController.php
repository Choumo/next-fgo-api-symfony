<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Symfony\Component\Security\Http\Attribute\IsGranted;

//TODO: ERREUR
#[Route('/api/summon')]
class SummonController extends AbstractController
{
    #[Route('/', name: 'app_summon', methods: ["POST"])]
    #[IsGranted('ROLE_USER')]
    public function index(Request $request): JsonResponse
    {
        try {
            $files = $request->files->get('images');

            if(!$files) {
                throw new Exception("No images uploaded");
            }

            /** @var UploadedFile $currentFile */
            foreach($files as $currentFile) {
                $tesseractOCR = new TesseractOCR($currentFile->getRealPath());

                dd($tesseractOCR->run());

            }
        } catch(Exception $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
