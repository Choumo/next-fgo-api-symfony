<?php

namespace App\Service\Statistics\Global;

use App\Service\Story\ChapterState;

class StoryStatistics
{
    private int $clearedChapters = 0;

    private int $inProgressChapters = 0;

    private int $notDoneChapters = 0;

    private array $firstArc;

    private array $secondArc;

    private array $thirdArc;

    private array $fourthArc;

    public function __construct() {
        $this->firstArc = [
            'Cleared' => 0,
            'In Progress' => 0,
            'Not Done' => 0,
        ];

        $this->secondArc = [
            'Cleared' => 0,
            'In Progress' => 0,
            'Not Done' => 0,
        ];

        $this->thirdArc = [
            'Cleared' => 0,
            'In Progress' => 0,
            'Not Done' => 0,
        ];

        $this->fourthArc = [
            'Cleared' => 0,
            'In Progress' => 0,
            'Not Done' => 0,
        ];
    }

    /**
     * @return int
     */
    public function getClearedChapters(): int
    {
        return $this->clearedChapters;
    }

    /**
     * @param int $clearedChapters
     */
    public function setClearedChapters(int $clearedChapters): void
    {
        $this->clearedChapters = $this->clearedChapters + $clearedChapters;
    }

    /**
     * @return int
     */
    public function getInProgressChapters(): int
    {
        return $this->inProgressChapters;
    }

    /**
     * @param int $inProgressChapters
     */
    public function setInProgressChapters(int $inProgressChapters): void
    {
        $this->inProgressChapters = $this->inProgressChapters + $inProgressChapters;
    }

    /**
     * @return int
     */
    public function getNotDoneChapters(): int
    {
        return $this->notDoneChapters;
    }

    /**
     * @param int $notDoneChapters
     */
    public function setNotDoneChapters(int $notDoneChapters): void
    {
        $this->notDoneChapters = $this->notDoneChapters + $notDoneChapters;
    }

    /**
     * @return array
     */
    public function getFirstArc(): array
    {
        return $this->firstArc;
    }

    /**
     * @param string|null $state
     */
    public function setFirstArc(?string $state): void
    {
        switch ($state) {
            case "Cleared":
                $this->firstArc['Cleared'] = $this->firstArc['Cleared'] + 1;
                $this->clearedChapters = $this->clearedChapters + 1;
                break;
            case "In Progress":
                $this->firstArc['In Progress'] = $this->firstArc['In Progress'] + 1;
                $this->inProgressChapters = $this->inProgressChapters + 1;
                break;
            default:
                $this->firstArc['Not Done'] = $this->firstArc['Not Done'] + 1;
                $this->notDoneChapters = $this->notDoneChapters + 1;
                break;
        }
    }

    /**
     * @return array
     */
    public function getSecondArc(): array
    {
        return $this->secondArc;
    }

    /**
     * @param string|null $state
     */
    public function setSecondArc(?string $state): void
    {
        switch ($state) {
            case "Cleared":
                $this->secondArc['Cleared'] = $this->secondArc['Cleared'] + 1;
                $this->clearedChapters = $this->clearedChapters + 1;
                break;
            case "In Progress":
                $this->secondArc['In Progress'] = $this->secondArc['In Progress'] + 1;
                $this->inProgressChapters = $this->inProgressChapters + 1;
                break;
            default:
                $this->secondArc['Not Done'] = $this->secondArc['Not Done'] + 1;
                $this->notDoneChapters = $this->notDoneChapters + 1;
                break;
        }
    }

    /**
     * @return array
     */
    public function getThirdArc(): array
    {
        return $this->thirdArc;
    }

    /**
     * @param string|null $state
     */
    public function setThirdArc(?string $state): void
    {
        switch ($state) {
            case "Cleared":
                $this->thirdArc['Cleared'] = $this->thirdArc['Cleared'] + 1;
                $this->clearedChapters = $this->clearedChapters + 1;
                break;
            case "In Progress":
                $this->thirdArc['In Progress'] = $this->thirdArc['In Progress'] + 1;
                $this->inProgressChapters = $this->inProgressChapters + 1;
                break;
            default:
                $this->thirdArc['Not Done'] = $this->thirdArc['Not Done'] + 1;
                $this->notDoneChapters = $this->notDoneChapters + 1;
                break;
        }
    }

    /**
     * @return array
     */
    public function getFourthArc(): array
    {
        return $this->fourthArc;
    }

    /**
     * @param string|null $state
     */
    public function setFourthArc(?string $state): void
    {
        switch ($state) {
            case "Cleared":
                $this->fourthArc['Cleared'] = $this->fourthArc['Cleared'] + 1;
                $this->clearedChapters = $this->clearedChapters + 1;
                break;
            case "In Progress":
                $this->fourthArc['In Progress'] = $this->fourthArc['In Progress'] + 1;
                $this->inProgressChapters = $this->inProgressChapters + 1;
                break;
            default:
                $this->fourthArc['Not Done'] = $this->fourthArc['Not Done'] + 1;
                $this->notDoneChapters = $this->notDoneChapters + 1;
                break;
        }
    }

    public function getStatistics():  array
    {
        return [
            'totalClearedChapters' => $this->clearedChapters,
            'totalInProgressChapters' => $this->inProgressChapters,
            'totalNotDoneChapters' => $this->notDoneChapters,
            'totalFirstArc' => $this->firstArc,
            'totalSecondArc' => $this->secondArc,
            'totalThirdArc' => $this->thirdArc,
            'totalFourthArc' => $this->fourthArc,
        ];
    }


}
