<?php

namespace App\Tests\Controller\CraftEssences;

use App\Repository\UserRepository;

class UserCraftEssenceTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    public function testShowListOfServantsOfUserIfUserLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->loginUser($user);
        $response_craft_essence_list = $client->request("GET", "/craft-essence/user/list");

        $this->assertResponseIsSuccessful();
    }

    public function testShowListOfServantsOfUserIfUserNotLogged(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        $userRepo = $container->get(UserRepository::class);
        $user = $userRepo->findOneBy(["email" => "email@provider.fr"]);

        $client->request("GET", "/craft-essence/user/list");

        $this->assertResponseStatusCodeSame(401);
    }
}
