<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Serializer\Normalizer\ConstraintViolationListNormalizer;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ApiValidationException extends \RuntimeException
{
    private readonly ConstraintViolationListInterface $constraintViolationList;

    public function __construct(ConstraintViolationListInterface $constraintViolationList)
    {
        $this->constraintViolationList = $constraintViolationList;
        parent::__construct();
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }

    public function getDisplayableViolations(): array
    {
        $errors = [];

        foreach ($this->constraintViolationList as $violation) {
            $errors[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $errors;
    }
}
