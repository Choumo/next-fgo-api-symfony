<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class UserChangePasswordRequest extends AbstractRequest
{
    #[SerializedName('password')]
    #[Assert\PasswordStrength(['minScore' => Assert\PasswordStrength::STRENGTH_MEDIUM])]
    #[Assert\NotBlank]
    private string $password;

    #[SerializedName('passwordCheck')]
    #[Assert\EqualTo(propertyPath: 'password')]
    #[Assert\NotBlank]
    private string $passwordCheck;

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getPasswordCheck(): string
    {
        return $this->passwordCheck;
    }

    public function setPasswordCheck(string $passwordCheck): void
    {
        $this->passwordCheck = $passwordCheck;
    }


}
