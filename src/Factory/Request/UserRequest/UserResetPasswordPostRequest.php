<?php

namespace App\Factory\Request\UserRequest;

use App\Factory\Request\AbstractRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class UserResetPasswordPostRequest extends AbstractRequest
{
    #[SerializedName('email')]
    #[Assert\Email]
    #[Assert\NotBlank]
    private string $email;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }


}
