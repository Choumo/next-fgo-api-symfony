<?php

namespace App\Controller;

use App\Entity\Servant;
use App\Entity\User;
use App\Exception\ApiException;
use App\Exception\ApiValidationException;
use App\Factory\Request\ServantRequest\ServantPatchRequest;
use App\Repository\ServantRepository;
use App\Repository\UserRepository;
use App\Service\Api;
use App\Service\JsonTransformer;
use App\Service\Statistics\Global\ServantStatistics;
use Doctrine\ORM\EntityManagerInterface;

use SensitiveParameter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedJsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/servants')]
class ServantController extends AbstractController
{

    public function __construct(private readonly JsonTransformer $jsonTransformer, private readonly ValidatorInterface $validator,) {}

    /**
     * Fonction qui permet de récupérer la liste des servants
     *
     * @param Api $rayshiftApi
     * @param ServantRepository $servantRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/list', name: 'app_servant_list', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function index(Api $rayshiftApi, ServantRepository $servantRepository, #[CurrentUser] User $user): JsonResponse
    {
        try {
            $servant_array = [];

            $response = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_servant_lang_en.json', cacheKey: 'servant-list');

            $count_servants = count($response);

            for ($i = 0; $i < $count_servants; $i++) {
                if ($response[$i]['type'] === 'enemyCollectionDetail') {
                    continue;
                } else {
                    $user_has_servant = $servantRepository->findOneBy(["servant_id" => $response[$i]['collectionNo'], "user" => $user]);
                    $servant_array[] = [
                        'id' => $response[$i]['collectionNo'],
                        'name' => $response[$i]['name'],
                        'class' => $response[$i]['className'],
                        'rarity' => $response[$i]['rarity'],
                        'graph' => $response[$i]['extraAssets']['charaGraph']['ascension'][4],
                        'isObtained' => (bool)$user_has_servant
                    ];
                }
            }

            return new JsonResponse(
                data: $servant_array,
                status: Response::HTTP_OK
            );
        } catch (ApiException) {
            return new JsonResponse(
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet d'ajouter un servant à l'inventaire de l'utilisateur connecté
     *
     * @param int $id
     * @param ServantRepository $servantRepository
     * @param EntityManagerInterface $entityManager
     * @param User $user
     * @param Api $rayshiftApi
     * @return JsonResponse
     */
    #[Route('/add/{id}', name: 'app_servant_add', methods: ["POST"])]
    #[IsGranted('ROLE_USER')]
    public function addServantToUser(#[SensitiveParameter] int $id, ServantRepository $servantRepository, EntityManagerInterface $entityManager, #[CurrentUser] User $user, Api $rayshiftApi): JsonResponse
    {
        try {
            $servant = $servantRepository->findOneBy(["servant_id" => $id, 'user' => $user]);
            $response = $rayshiftApi->callRayshiftApi(endpoint: 'nice/JP/servant/'. $id .'?lang=en', cacheKey: 'servant-'.$id);

            if ($servant) {
                $entityManager->remove($servant);
                $entityManager->flush();

                return new JsonResponse(
                    data: [
                        "status" => "success",
                        "message" => "ServantObject deleted from your database."
                    ],
                    status: Response::HTTP_CREATED
                );
            } else {
                $newServant = new Servant();
                $newServant->setServantId($id);
                $newServant->setUser($this->getUser());

                $entityManager->persist($newServant);
                $entityManager->flush();

                return new JsonResponse(
                    data: [
                        "status" => "success",
                        "message" => "ServantObject added to your database."
                    ],
                    status: Response::HTTP_CREATED
                );
            }
        }catch(ApiException $e){
            return new JsonResponse(
                data: [
                    "status" => "error",
                    "message" => $e->getMessage()
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de récupérer la liste des servants pour l'utilisateur connecté
     *
     * @param Api $rayshift_api
     * @param ServantRepository $servantRepository
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/user/list', name: 'app_show_user_servants', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function listAllServantsOfUser(Api $rayshift_api, ServantRepository $servantRepository, #[CurrentUser] User $user): JsonResponse
    {
       try {
            $user_servants = [];

            $listServants = $servantRepository->findBy(["user" => $user]);

            foreach($listServants as $currentServant){
                $servant_info = $rayshift_api->callRayshiftApi(endpoint: 'nice/JP/servant/' . $currentServant->getServantId() . '?lang=en', cacheKey: 'servant-'.$currentServant->getServantId());
                $user_servants[] = [
                    'id' => $servant_info['collectionNo'],
                    'name' => $servant_info['name'],
                    'class' => $servant_info['className'],
                    'rarity' => $servant_info['rarity'],
                    'graph' => $servant_info['extraAssets']['charaGraph']['ascension'][4],
                    'level' => $currentServant->getServantLevel(),
                    'servant_skill_1' => $currentServant->getServantLevelSkill1(),
                    'servant_skill_2' => $currentServant->getServantLevelSkill2(),
                    'servant_skill_3' => $currentServant->getServantLevelSkill3(),
                    'servant_append_1' => $currentServant->getServantLevelAppend1(),
                    'servant_append_2' => $currentServant->getServantLevelAppend2(),
                    'servant_append_3' => $currentServant->getServantLevelAppend3(),
                ];
            }

            return new JsonResponse(
                data: $user_servants,
                status: Response::HTTP_OK
            );
        }catch(ApiException $e){
            return new JsonResponse(
                data: ["status" => "error", "message" => $e->getMessage()],
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de récupérer les informations d'un servant de l'utilisateur connecté
     *
     * @param int $id
     * @param EntityManagerInterface $entityManager
     * @param User $user
     * @param Api $rayshiftApi
     * @return JsonResponse
     */
    #[Route('/user/{id}', name: 'app_user_servant_data', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function getServantUserData(#[SensitiveParameter] int $id, EntityManagerInterface $entityManager, #[CurrentUser] User $user, Api $rayshiftApi): JsonResponse
    {
        try {
            $apiServantData = $rayshiftApi->callRayshiftApi(endpoint:'nice/JP/servant/' . $id . '?lang=en', cacheKey: 'servant-'.$id);

            $servantRepository = $entityManager->getRepository(Servant::class);
            $userServantData = $servantRepository->findOneBy(["servant_id" => $id, "user" => $user]);

            $currentServantData = [
                'id' => $apiServantData['collectionNo'],
                'name' => $apiServantData['name'],
                'class' => $apiServantData['className'],
                'rarity' => $apiServantData['rarity'],
                'graph' => $apiServantData['extraAssets']['charaGraph']['ascension'][4],
                'level' => $userServantData->getServantLevel(),
                'bond_level' => $userServantData->getServantBondLevel(),
                'np_level' => $userServantData->getServantNpLevel(),
                'servant_skill_1' => [
                    "skill_level" => $userServantData->getServantLevelSkill1(),
                    "skill_name" => $apiServantData['skills'][0]['name'],
                    "skill_icon" => $apiServantData['skills'][0]['icon'],
                ],
                'servant_skill_2' => [
                    "skill_level" => $userServantData->getServantLevelSkill2(),
                    "skill_name" => $apiServantData['skills'][1]['name'],
                    "skill_icon" => $apiServantData['skills'][1]['icon'],
                ],
                'servant_skill_3' => [
                    "skill_level" => $userServantData->getServantLevelSkill2(),
                    "skill_name" => $apiServantData['skills'][2]['name'],
                    "skill_icon" => $apiServantData['skills'][2]['icon'],
                ],
                'servant_append_1' => [
                    "append_level" => $userServantData->getServantLevelAppend1(),
                    "append_name" => $apiServantData['appendPassive'][0]['skill']['name'],
                    "append_icon" => $apiServantData['appendPassive'][0]['skill']['icon'],
                ],
                'servant_append_2' => [
                    "append_level" => $userServantData->getServantLevelAppend2(),
                    "append_name" => $apiServantData['appendPassive'][1]['skill']['name'],
                    "append_icon" => $apiServantData['appendPassive'][1]['skill']['icon'],
                ],
                'servant_append_3' => [
                    "append_level" => $userServantData->getServantLevelAppend3(),
                    "append_name" => $apiServantData['appendPassive'][2]['skill']['name'],
                    "append_icon" => $apiServantData['appendPassive'][2]['skill']['icon'],
                ],
            ];

            return new JsonResponse(
                data: $currentServantData,
                status: Response::HTTP_OK
            );
        }catch (ApiException $exception){
            return new JsonResponse(
                data: [
                    "status" => "error",
                    "message" => $exception->getMessage()
                ],status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet à un utilisateur de mettre à jour les informations d'un servant
     *
     * @param int $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param User $user
     * @return JsonResponse
     */
    #[Route('/user/{id}/update', methods: ["PATCH"])]
    #[IsGranted('ROLE_USER')]
    public function updateServantInformationOfuser(int $id, Request $request, EntityManagerInterface $entityManager, #[CurrentUser] User $user): JsonResponse
    {
        try{
            /** @var ServantPatchRequest|null $requestPayload */
            $requestPayload = $this->jsonTransformer->deserializeJson(json: $request->getContent(), class: ServantPatchRequest::class);

            if (null === $requestPayload) {
                return new JsonResponse(
                    data: [
                        "Impossible de désérialiser le contenu de la requête"
                    ],
                    status: Response::HTTP_UNPROCESSABLE_ENTITY,
                );
            }

            try {
                $payload = $requestPayload->validate($this->validator);
            } catch (ApiValidationException $exception) {
                return new JsonResponse(
                    data: $exception->getDisplayableViolations(),
                    status: Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            $servantRepository = $entityManager->getRepository(Servant::class);
            $servantToUpdate = $servantRepository->findOneBy([
               "user" => $user,
               "servant_id" => $id
            ]);

            $servantToUpdate->setServantLevel($requestPayload->getLevel());
            $servantToUpdate->setServantLevelSkill1($requestPayload->getServantSkill1());
            $servantToUpdate->setServantLevelSkill2($requestPayload->getServantSkill2());
            $servantToUpdate->setServantLevelSkill3($requestPayload->getServantSkill3());
            $servantToUpdate->setServantLevelAppend1($requestPayload->getServantAppend1());
            $servantToUpdate->setServantLevelAppend2($requestPayload->getServantAppend2());
            $servantToUpdate->setServantLevelAppend3($requestPayload->getServantAppend3());
            $servantToUpdate->setServantBondLevel($requestPayload->getBondLevel());
            $servantToUpdate->setServantNpLevel($requestPayload->getNpLevel());


            $entityManager->persist($servantToUpdate);
            $entityManager->flush();

            return new JsonResponse(
                data: [
                    "status" => "success",
                    "message" => "The servant data has been updated"
                ],
                status: Response::HTTP_NO_CONTENT
            );
        }catch(ApiException $e){
            return new JsonResponse(
                data: [
                    "status" => "error",
                    "message" => $e->getMessage()
                ],
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de récupérer les statistiques en lien avec les servants de l'utilisateur connecté
     *
     * @param User $user
     * @param ServantRepository $servantRepository
     * @param Api $rayshiftApi
     * @return JsonResponse
     */
    #[Route('/statistics', name: 'app_servant_user_statistics', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function userStatistics(#[CurrentUser] User $user, ServantRepository $servantRepository, Api $rayshiftApi): JsonResponse
    {
        try {
            $servantsOfUser = $servantRepository->findBy(["user" => $user]);
            $servantsOfApi = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_servant_lang_en.json', cacheKey: 'servant-list');

            $fiveStarsApiServants = 0;
            $fourStarsApiServants = 0;
            $threeStarsApiServants = 0;
            $twoStarsApiServants = 0;
            $oneStarsApiServants = 0;

            $fiveStarsUserServants = 0;
            $fourStarsUserServants = 0;
            $threeStarsUserServants = 0;
            $twoStarsUserServants = 0;
            $oneStarsUserServants = 0;

            $servantByClass = [
                "saber" => 0,
                "archer" => 0,
                "lancer" => 0,
                "rider" => 0,
                "caster" => 0,
                "assassin" => 0,
                "berserker" => 0,
                "ruler" => 0,
                "avenger" => 0,
                "moonCancer" => 0,
                "alterEgo" => 0,
                "foreigner" => 0,
                "pretender" => 0,
                "shielder" => 0,
                "beast" => 0,
            ];

            foreach ($servantsOfApi as $servant) {
                if($servant["type"] === 'enemyCollectionDetail') {
                    continue;
                }

                switch ($servant["rarity"]) {
                    case 5:
                        $fiveStarsApiServants = $fiveStarsApiServants + 1;
                        break;
                    case 4:
                        $fourStarsApiServants = $fourStarsApiServants + 1;
                        break;
                    case 3:
                        $threeStarsApiServants = $threeStarsApiServants + 1;
                        break;
                    case 2:
                        $twoStarsApiServants = $twoStarsApiServants + 1;
                        break;
                    case 1:
                        $oneStarsApiServants = $oneStarsApiServants + 1;
                        break;
                }

                foreach ($servantsOfUser as $userServant) {
                    if($userServant->getServantId() === $servant["collectionNo"]) {
                        switch ($servant["rarity"]) {
                            case 5:
                                $fiveStarsUserServants = $fiveStarsUserServants + 1;
                                break;
                            case 4:
                                $fourStarsUserServants = $fourStarsUserServants + 1;
                                break;
                            case 3:
                                $threeStarsUserServants = $threeStarsUserServants + 1;
                                break;
                            case 2:
                                $twoStarsUserServants = $twoStarsUserServants + 1;
                                break;
                            case 1:
                                $oneStarsUserServants = $oneStarsUserServants + 1;
                                break;
                        }

                        $servantByClass[$servant["className"]] = $servantByClass[$servant["className"]] + 1;
                    }
                }
            }

            $userStatistics = [
                "totalServantObtained" => count($servantsOfUser),
                "totalServantUnobtained" => (count($servantsOfApi) - count($servantsOfUser)),
                "totalServantObtainedPercentage" => round((count($servantsOfUser) / count($servantsOfApi) * 100)),
                "servantRarityProgress" => [
                    "fiveStars" => [
                        "user" => $fiveStarsUserServants,
                        "game" => $fiveStarsApiServants,
                    ],
                    "fourStars" => [
                        "user" => $fourStarsUserServants,
                        "game" => $fourStarsApiServants,
                    ],
                    "threeStars" => [
                        "user" => $threeStarsUserServants,
                        "game" => $threeStarsApiServants,
                    ],
                    "twoStars" => [
                        "user" => $twoStarsUserServants,
                        "game" => $twoStarsApiServants,
                    ],
                    "oneStars" => [
                        "user" => $oneStarsUserServants,
                        "game" => $oneStarsApiServants,
                    ],
                ],
                "servantClassStatistics" => $servantByClass
            ];

            return new JsonResponse(
                data: $userStatistics,
                status: Response::HTTP_OK
            );
        } catch (ApiException $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Fonction qui permet de récupérer les statistiques globales de tout les utilisateur pour les servants
     *
     * @param ServantRepository $servantRepository
     * @param Api $rayshiftApi
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    #[Route('/global-statistics', name: 'app_servant_global_statistics', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function globalStatistics(ServantRepository $servantRepository, Api $rayshiftApi, UserRepository $userRepository): JsonResponse
    {
        try {
            $users = $userRepository->findAll();
            $servantsOfApi = $rayshiftApi->callRayshiftApi(endpoint: 'export/JP/nice_servant_lang_en.json', cacheKey: 'servant-list');
            $servantStatistics = new ServantStatistics();

            foreach ($users as $user) {
                if($user->isShareData() === true) {
                    $servantsOfUser = $user->getServants();
                    foreach ($servantsOfApi as $apiServant) {
                        if ($apiServant["type"] === 'enemyCollectionDetail') {
                            continue;
                        }

                        foreach ($servantsOfUser as $userServant) {
                            if ($apiServant['collectionNo'] === $userServant->getServantId()) {
                                switch ($apiServant["rarity"]) {
                                    case 5:
                                        $servantStatistics->setFiveStar(fiveStar: 1);
                                        break;
                                    case 4:
                                        $servantStatistics->setFourStar(fourStar: 1);
                                        break;
                                    case 3:
                                        $servantStatistics->setThreeStar(threeStar: 1);
                                        break;
                                    case 2:
                                        $servantStatistics->setTwoStar(twoStar: 1);
                                        break;
                                    case 1:
                                        $servantStatistics->setOneStar(oneStar: 1);
                                        break;
                                }

                                $servantStatistics->setServantClass($apiServant["className"], 1);
                            }
                        }
                    }
                }
            }

            return new JsonResponse(
                data: $servantStatistics->getStatistics(),
                status: Response::HTTP_OK
            );

        } catch (\Exception|ApiException $exception) {
            return new JsonResponse(
                data: $exception->getMessage(),
                status: Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
