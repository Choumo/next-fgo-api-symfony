<?php

namespace App\Service;

use App\Exception\ApiException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

ini_set("memory_limit", -1);

class Api {

    private string $rayshiftApiUrl;

    public function __construct(
        private readonly HttpClientInterface $client,
        ParameterBagInterface $parameterBag,
        private CacheInterface $cache
    ) {
        $this->rayshiftApiUrl = $parameterBag->get('app.rayshift_api_url');
    }

    /**
     * Fonction qui permet d'effectuer des appels à l'API d'Atlas Academy
     *
     * @param string $endpoint
     * @param string $cacheKey
     * @return array
     */
    public function callRayshiftApi(string $endpoint, string $cacheKey): array {
            $content = $this->cache->get($cacheKey, function (ItemInterface $item) use ($endpoint){
            $item->expiresAfter(3600);
            $response = $this->client->request(
                method: 'GET',
                url: $this->rayshiftApiUrl . $endpoint,
                options: [
                    'buffer' => false
                ]);

            switch ($response->getStatusCode()) {
                case 500:
                case 422:
                case 403:
                case 400:
                    throw new ApiException(message: "Erreur");
                default:
                    return $response->getContent();
            }
        });

        return json_decode(json: $content, associative: true);
    }
}
